Imports System.Data.SqlClient

Public Class FrmPago
    Public BndLocal As Boolean = True
    Dim Bndnoentres As Boolean = False
    Dim monto As Double
    Private Sub Llena_Combo()
        Dim CON As New SqlConnection(MiConexion)
        Dim CMD As New SqlCommand()
        Try
            CMD = New SqlCommand()
            CON.Open()
            With CMD
                .CommandText = "Muestra_Tipos_Tarjeta"
                .CommandTimeout = 0
                .Connection = CON
                .CommandType = CommandType.StoredProcedure

                Dim PRM As New SqlParameter("@OP", SqlDbType.Int)
                PRM.Value = 0
                PRM.Direction = ParameterDirection.Input
                .Parameters.Add(PRM)

                Dim READER As SqlDataReader = .ExecuteReader()

                While (READER.Read)
                    Me.CmbTipCuenta.Items.Add(READER.GetValue(1))
                End While
            End With
            CON.Close()
        Catch ex As Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub EfectivoTextBox_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles EfectivoTextBox.Click
        Me.EfectivoTextBox.SelectionStart = 0
        Me.EfectivoTextBox.SelectionLength = Len(Me.EfectivoTextBox.Text)
    End Sub

    Private Sub EfectivoTextBox_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles EfectivoTextBox.GotFocus
        Me.EfectivoTextBox.SelectionStart = 0
        Me.EfectivoTextBox.SelectionLength = Len(Me.EfectivoTextBox.Text)
    End Sub

    Private Sub TextBox1_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles EfectivoTextBox.TextChanged
        Suma(0)
    End Sub

    Private Sub Label4_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Label4.Click

    End Sub

    Private Sub Suma(ByVal TipoPago As Integer)
        If BndLocal = True Then
            Dim Efectivo As Double = 0
            Dim cheque As Double = 0
            Dim Tarjeta As Double = 0
            Dim Nota As Double = 0
            Dim Cambio As Double = 0
            Dim Total As Double = 0
            Dim SubTotal As Double = 0
            Dim saldo As Double = 0
            Dim Abono As Double = 0
            Dim Transferencia As Double = 0

            '0 --Efectivo
            '1 --Cheque
            '2 --Tarjeta
            If IsNumeric(Me.CMBTotalaPagarLabel.Text) = False Then Total = 0 Else Total = Me.CMBTotalaPagarLabel.Text
            If IsNumeric(Me.EfectivoTextBox.Text) = False Then Efectivo = 0 Else Efectivo = Me.EfectivoTextBox.Text
            If IsNumeric(Me.ChequeTextBox.Text) = False Then cheque = 0 Else cheque = Me.ChequeTextBox.Text
            If IsNumeric(Me.TextBox4.Text) = False Then Nota = 0 Else Nota = Me.TextBox4.Text
            If IsNumeric(Me.TarjetaTextBox.Text) = False Then Tarjeta = 0 Else Tarjeta = Me.TarjetaTextBox.Text
            If IsNumeric(Me.CMBSaldoLabel.Text) = False Then saldo = 0 Else saldo = Me.CMBSaldoLabel.Text
            If IsNumeric(Me.CMBAbonoLabel.Text) = False Then Abono = 0 Else Abono = Me.CMBAbonoLabel.Text
            If IsNumeric(tbTransferencia.Text) = False Then Transferencia = 0 Else Transferencia = tbTransferencia.Text
            '
            Select Case TipoPago
                Case 0 'Efectivo
                    SubTotal = Total - (cheque + Tarjeta + Nota + Transferencia)
                Case 1 'chaque
                    SubTotal = Total - (Efectivo + Tarjeta + Nota + Transferencia)
                Case 2 'tarjeta
                    SubTotal = Total - (Efectivo + cheque + Nota + Transferencia)
                Case 3 'Nota
                    SubTotal = Total - (Efectivo + cheque + Tarjeta + Transferencia)
                Case 4
                    SubTotal = Total - (Efectivo + cheque + Tarjeta + Nota)
            End Select


            If SubTotal < 0 Then
                saldo = 0
                Abono = Total
            Else
                saldo = SubTotal
                Abono = Total - SubTotal
            End If

            Select Case TipoPago
                Case 0 'Efectivo
                    If saldo > 0 Then
                        SubTotal = SubTotal - Efectivo
                        If SubTotal < 0 Then
                            Cambio = (SubTotal * -1)
                        Else
                            Cambio = 0
                        End If
                        If SubTotal < 0 Then
                            saldo = 0
                            Abono = Total
                        Else
                            saldo = SubTotal
                            Abono = Total - SubTotal
                        End If

                    Else
                        Me.EfectivoTextBox.Text = 0
                        Cambio = 0
                    End If
                Case 1 'cheque
                    If saldo > 0 Then
                        SubTotal = SubTotal - cheque
                        If SubTotal < 0 Then
                            'MsgBox("La Cantidad Capturada no es Valida", MsgBoxStyle.Information)
                            Me.ChequeTextBox.Text = 0
                            Cambio = 0
                        Else
                            If SubTotal < 0 Then
                                saldo = 0
                                Abono = Total
                            Else
                                saldo = SubTotal
                                Abono = Total - SubTotal
                            End If
                            Cambio = 0
                        End If
                    Else
                        BndLocal = False
                        Me.ChequeTextBox.Text = 0
                        cheque = 0
                        BndLocal = True

                        SubTotal = Total - (cheque + Tarjeta + Nota + Transferencia)
                        If SubTotal < 0 Then
                            saldo = 0
                            Abono = Total
                        Else
                            saldo = SubTotal
                            Abono = Total - SubTotal
                        End If
                        If saldo > 0 Then
                            SubTotal = SubTotal - Efectivo
                            If SubTotal < 0 Then
                                Cambio = (SubTotal * -1)
                            Else
                                Cambio = 0
                            End If
                            If SubTotal < 0 Then
                                saldo = 0
                                Abono = Total
                            Else
                                saldo = SubTotal
                                Abono = Total - SubTotal
                            End If

                        Else
                            Me.EfectivoTextBox.Text = 0
                            Cambio = 0
                        End If
                        MsgBox(SubTotal)
                        Me.CMBCambioLabel.Text = Cambio
                        Me.CMBAbonoLabel.Text = Abono
                        Me.CMBSaldoLabel.Text = saldo

                        Exit Sub
                        BndLocal = True
                        'Cambio = 0
                    End If
                Case 2 'tarjeta
                    If saldo > 0 Then
                        SubTotal = SubTotal - Tarjeta
                        If SubTotal < 0 Then
                            'MsgBox("La Cantidad Capturada no es Valida", MsgBoxStyle.Information)
                            Me.TarjetaTextBox.Text = 0
                            Cambio = 0
                        Else
                            If SubTotal < 0 Then
                                saldo = 0
                                Abono = Total
                            Else
                                saldo = SubTotal
                                Abono = Total - SubTotal
                            End If
                            Cambio = 0
                        End If
                    Else
                        BndLocal = False
                        Me.TarjetaTextBox.Text = 0
                        Tarjeta = 0
                        BndLocal = True
                        SubTotal = Total - (cheque + Tarjeta + Nota + Transferencia)
                        If SubTotal < 0 Then
                            saldo = 0
                            Abono = Total
                        Else
                            saldo = SubTotal
                            Abono = Total - SubTotal
                        End If
                        If saldo > 0 Then
                            SubTotal = SubTotal - Efectivo
                            If SubTotal < 0 Then
                                Cambio = (SubTotal * -1)
                            Else
                                Cambio = 0
                            End If
                            If SubTotal < 0 Then
                                saldo = 0
                                Abono = Total
                            Else
                                saldo = SubTotal
                                Abono = Total - SubTotal
                            End If

                        Else
                            Me.EfectivoTextBox.Text = 0
                            Cambio = 0
                        End If
                        Me.CMBCambioLabel.Text = Cambio
                        Me.CMBAbonoLabel.Text = Abono
                        Me.CMBSaldoLabel.Text = saldo

                        Exit Sub
                        BndLocal = True
                        'Cambio = 0
                    End If
                Case 3 'Nota de Credito
                    If saldo > 0 Then
                        If Nota > SubTotal And Nota > 0 Then
                            BndLocal = False
                            Me.TextBox4.Text = SubTotal
                            Nota = SubTotal
                            BndLocal = True
                        End If

                        SubTotal = SubTotal - Nota
                        If SubTotal < 0 Then
                            'MsgBox("La Cantidad Capturada no es Valida", MsgBoxStyle.Information)
                            Me.TextBox4.Text = 0
                            Cambio = 0
                        Else
                            If SubTotal < 0 Then
                                saldo = 0
                                Abono = Total
                            Else
                                saldo = SubTotal
                                Abono = Total - SubTotal
                            End If
                            Cambio = 0
                        End If
                    Else
                        BndLocal = False
                        Me.TextBox4.Text = 0
                        Nota = 0
                        BndLocal = True
                        SubTotal = Total - (cheque + Tarjeta + Nota + Transferencia)
                        If SubTotal < 0 Then
                            saldo = 0
                            Abono = Total
                        Else
                            saldo = SubTotal
                            Abono = Total - SubTotal
                        End If
                        If saldo > 0 Then
                            SubTotal = SubTotal - Efectivo
                            If SubTotal < 0 Then
                                Cambio = (SubTotal * -1)
                            Else
                                Cambio = 0
                            End If
                            If SubTotal < 0 Then
                                saldo = 0
                                Abono = Total
                            Else
                                saldo = SubTotal
                                Abono = Total - SubTotal
                            End If

                        Else
                            Me.EfectivoTextBox.Text = 0
                            Cambio = 0
                        End If
                        Me.CMBCambioLabel.Text = Cambio
                        Me.CMBAbonoLabel.Text = Abono
                        Me.CMBSaldoLabel.Text = saldo
                        Exit Sub
                        BndLocal = True
                        'Cambio = 0
                    End If
                Case 4
                    If saldo > 0 Then
                        SubTotal = SubTotal - Transferencia
                        If SubTotal < 0 Then
                            tbTransferencia.Text = 0
                            Cambio = 0
                        Else
                            If SubTotal < 0 Then
                                saldo = 0
                                Abono = Total
                            Else
                                saldo = SubTotal
                                Abono = Total - SubTotal
                            End If
                            Cambio = 0
                        End If
                    Else
                        BndLocal = False
                        tbTransferencia.Text = 0
                        Transferencia = 0
                        BndLocal = True
                        SubTotal = Total - (cheque + Tarjeta + Nota + Transferencia)
                        If SubTotal < 0 Then
                            saldo = 0
                            Abono = Total
                        Else
                            saldo = SubTotal
                            Abono = Total - SubTotal
                        End If
                        If saldo > 0 Then
                            SubTotal = SubTotal - Efectivo
                            If SubTotal < 0 Then
                                Cambio = (SubTotal * -1)
                            Else
                                Cambio = 0
                            End If
                            If SubTotal < 0 Then
                                saldo = 0
                                Abono = Total
                            Else
                                saldo = SubTotal
                                Abono = Total - SubTotal
                            End If
                        Else
                            Me.EfectivoTextBox.Text = 0
                            Cambio = 0
                        End If
                        Me.CMBCambioLabel.Text = Cambio
                        Me.CMBAbonoLabel.Text = Abono
                        Me.CMBSaldoLabel.Text = saldo

                        Exit Sub
                        BndLocal = True
                    End If
            End Select

            Me.CMBCambioLabel.Text = Format(Cambio, "##,##0.00")
            Me.CMBAbonoLabel.Text = Format(Abono, "##,##0.00")
            Me.CMBSaldoLabel.Text = Format(saldo, "##,##0.00")
            GloPagorecibido = (Cambio + Abono)
        End If
    End Sub


    'Private Sub Suma(ByVal TipoPago As Integer)
    '    If BndLocal = True Then
    '        Dim Efectivo As Double = 0
    '        Dim cheque As Double = 0
    '        Dim Tarjeta As Double = 0
    '        Dim Nota As Double = 0
    '        Dim Cambio As Double = 0
    '        Dim Total As Double = 0
    '        Dim SubTotal As Double = 0
    '        Dim saldo As Double = 0
    '        Dim Abono As Double = 0

    '        '0 --Efectivo
    '        '1 --Cheque
    '        '2 --Tarjeta
    '        If IsNumeric(Me.CMBTotalaPagarLabel.Text) = False Then Total = 0 Else Total = Me.CMBTotalaPagarLabel.Text
    '        If IsNumeric(Me.EfectivoTextBox.Text) = False Then Efectivo = 0 Else Efectivo = Me.EfectivoTextBox.Text
    '        If IsNumeric(Me.ChequeTextBox.Text) = False Then cheque = 0 Else cheque = Me.ChequeTextBox.Text
    '        If IsNumeric(Me.TextBox4.Text) = False Then Nota = 0 Else Nota = Me.TextBox4.Text
    '        If IsNumeric(Me.TarjetaTextBox.Text) = False Then Tarjeta = 0 Else Tarjeta = Me.TarjetaTextBox.Text
    '        If IsNumeric(Me.CMBSaldoLabel.Text) = False Then saldo = 0 Else saldo = Me.CMBSaldoLabel.Text
    '        If IsNumeric(Me.CMBAbonoLabel.Text) = False Then Abono = 0 Else Abono = Me.CMBAbonoLabel.Text
    '        '
    '        Select Case TipoPago
    '            Case 0 'Efectivo
    '                SubTotal = Total - (cheque + Tarjeta + Nota)
    '            Case 1 'chaque
    '                SubTotal = Total - (Efectivo + Tarjeta + Nota)
    '            Case 2 'tarjeta
    '                SubTotal = Total - (Efectivo + cheque + Nota)
    '            Case 3 'Nota
    '                SubTotal = Total - (Efectivo + cheque + Tarjeta)
    '        End Select


    '        If SubTotal < 0 Then
    '            saldo = 0
    '            Abono = Total
    '        Else
    '            saldo = SubTotal
    '            Abono = Total - SubTotal
    '        End If

    '        Select Case TipoPago
    '            Case 0 'Efectivo
    '                If saldo > 0 Then
    '                    SubTotal = SubTotal - Efectivo
    '                    If SubTotal < 0 Then
    '                        Cambio = (SubTotal * -1)
    '                    Else
    '                        Cambio = 0
    '                    End If
    '                    If SubTotal < 0 Then
    '                        saldo = 0
    '                        Abono = Total
    '                    Else
    '                        saldo = SubTotal
    '                        Abono = Total - SubTotal
    '                    End If

    '                Else
    '                    Me.EfectivoTextBox.Text = 0
    '                    Cambio = 0
    '                End If
    '            Case 1 'cheque
    '                If saldo > 0 Then
    '                    SubTotal = SubTotal - cheque
    '                    If SubTotal < 0 Then
    '                        'MsgBox("La Cantidad Capturada no es Valida", MsgBoxStyle.Information)
    '                        Me.ChequeTextBox.Text = 0
    '                        Cambio = 0
    '                    Else
    '                        If SubTotal < 0 Then
    '                            saldo = 0
    '                            Abono = Total
    '                        Else
    '                            saldo = SubTotal
    '                            Abono = Total - SubTotal
    '                        End If
    '                        Cambio = 0
    '                    End If
    '                Else
    '                    BndLocal = False
    '                    Me.ChequeTextBox.Text = 0
    '                    cheque = 0
    '                    BndLocal = True

    '                    SubTotal = Total - (cheque + Tarjeta + Nota)
    '                    If SubTotal < 0 Then
    '                        saldo = 0
    '                        Abono = Total
    '                    Else
    '                        saldo = SubTotal
    '                        Abono = Total - SubTotal
    '                    End If
    '                    If saldo > 0 Then
    '                        SubTotal = SubTotal - Efectivo
    '                        If SubTotal < 0 Then
    '                            Cambio = (SubTotal * -1)
    '                        Else
    '                            Cambio = 0
    '                        End If
    '                        If SubTotal < 0 Then
    '                            saldo = 0
    '                            Abono = Total
    '                        Else
    '                            saldo = SubTotal
    '                            Abono = Total - SubTotal
    '                        End If

    '                    Else
    '                        Me.EfectivoTextBox.Text = 0
    '                        Cambio = 0
    '                    End If
    '                    MsgBox(SubTotal)
    '                    Me.CMBCambioLabel.Text = Cambio
    '                    Me.CMBAbonoLabel.Text = Abono
    '                    Me.CMBSaldoLabel.Text = saldo

    '                    Exit Sub
    '                    BndLocal = True
    '                    'Cambio = 0
    '                End If
    '            Case 2 'tarjeta
    '                If saldo > 0 Then
    '                    SubTotal = SubTotal - Tarjeta
    '                    If SubTotal < 0 Then
    '                        'MsgBox("La Cantidad Capturada no es Valida", MsgBoxStyle.Information)
    '                        Me.TarjetaTextBox.Text = 0
    '                        Cambio = 0
    '                    Else
    '                        If SubTotal < 0 Then
    '                            saldo = 0
    '                            Abono = Total
    '                        Else
    '                            saldo = SubTotal
    '                            Abono = Total - SubTotal
    '                        End If
    '                        Cambio = 0
    '                    End If
    '                Else
    '                    BndLocal = False
    '                    Me.TarjetaTextBox.Text = 0
    '                    Tarjeta = 0
    '                    BndLocal = True
    '                    SubTotal = Total - (cheque + Tarjeta + Nota)
    '                    If SubTotal < 0 Then
    '                        saldo = 0
    '                        Abono = Total
    '                    Else
    '                        saldo = SubTotal
    '                        Abono = Total - SubTotal
    '                    End If
    '                    If saldo > 0 Then
    '                        SubTotal = SubTotal - Efectivo
    '                        If SubTotal < 0 Then
    '                            Cambio = (SubTotal * -1)
    '                        Else
    '                            Cambio = 0
    '                        End If
    '                        If SubTotal < 0 Then
    '                            saldo = 0
    '                            Abono = Total
    '                        Else
    '                            saldo = SubTotal
    '                            Abono = Total - SubTotal
    '                        End If

    '                    Else
    '                        Me.EfectivoTextBox.Text = 0
    '                        Cambio = 0
    '                    End If
    '                    Me.CMBCambioLabel.Text = Cambio
    '                    Me.CMBAbonoLabel.Text = Abono
    '                    Me.CMBSaldoLabel.Text = saldo

    '                    Exit Sub
    '                    BndLocal = True
    '                    'Cambio = 0
    '                End If
    '            Case 3 'Nota de Credito
    '                If saldo > 0 Then
    '                    If Nota > SubTotal And Nota > 0 Then
    '                        BndLocal = False
    '                        Me.TextBox4.Text = SubTotal
    '                        Nota = SubTotal
    '                        BndLocal = True
    '                    End If

    '                    SubTotal = SubTotal - Nota
    '                    If SubTotal < 0 Then
    '                        'MsgBox("La Cantidad Capturada no es Valida", MsgBoxStyle.Information)
    '                        Me.TextBox4.Text = 0
    '                        Cambio = 0
    '                    Else
    '                        If SubTotal < 0 Then
    '                            saldo = 0
    '                            Abono = Total
    '                        Else
    '                            saldo = SubTotal
    '                            Abono = Total - SubTotal
    '                        End If
    '                        Cambio = 0
    '                    End If
    '                Else
    '                    BndLocal = False
    '                    Me.TextBox4.Text = 0
    '                    Nota = 0
    '                    BndLocal = True
    '                    SubTotal = Total - (cheque + Tarjeta + Nota)
    '                    If SubTotal < 0 Then
    '                        saldo = 0
    '                        Abono = Total
    '                    Else
    '                        saldo = SubTotal
    '                        Abono = Total - SubTotal
    '                    End If
    '                    If saldo > 0 Then
    '                        SubTotal = SubTotal - Efectivo
    '                        If SubTotal < 0 Then
    '                            Cambio = (SubTotal * -1)
    '                        Else
    '                            Cambio = 0
    '                        End If
    '                        If SubTotal < 0 Then
    '                            saldo = 0
    '                            Abono = Total
    '                        Else
    '                            saldo = SubTotal
    '                            Abono = Total - SubTotal
    '                        End If

    '                    Else
    '                        Me.EfectivoTextBox.Text = 0
    '                        Cambio = 0
    '                    End If
    '                    Me.CMBCambioLabel.Text = Cambio
    '                    Me.CMBAbonoLabel.Text = Abono
    '                    Me.CMBSaldoLabel.Text = saldo
    '                    Exit Sub
    '                    BndLocal = True
    '                    'Cambio = 0
    '                End If
    '        End Select

    '        Me.CMBCambioLabel.Text = Format(Cambio, "##,##0.00")
    '        Me.CMBAbonoLabel.Text = Format(Abono, "##,##0.00")
    '        Me.CMBSaldoLabel.Text = Format(saldo, "##,##0.00")
    '        GloPagorecibido = (Cambio + Abono)
    '    End If
    'End Sub

    Private Sub FrmPago_Activated(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Activated

    End Sub

    Private Sub FrmPago_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        colorea(Me)
        GloPagorecibido = 0
        GloTipoTarjeta = 0
        Me.CMBTotalaPagarLabel.Text = Format(GLOIMPTOTAL, "##,##0.00")
        'TODO: esta l�nea de c�digo carga datos en la tabla 'NewsoftvDataSet1.MUESTRABANCOS1' Puede moverla o quitarla seg�n sea necesario.
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        Me.MUESTRABANCOS1TableAdapter.Connection = CON
        Me.MUESTRABANCOS1TableAdapter.Fill(Me.NewsoftvDataSet1.MUESTRABANCOS1)

        'TODO: esta l�nea de c�digo carga datos en la tabla 'NewsoftvDataSet1.MUESTRABANCOS' Puede moverla o quitarla seg�n sea necesario.
        Me.MUESTRABANCOSTableAdapter.Connection = CON
        Me.MUESTRABANCOSTableAdapter.Fill(Me.NewsoftvDataSet1.MUESTRABANCOS)
        CON.Close()
        Llena_Combo()
        MUESTRABANCOS()
        'If SubCiudad <> "AG" Then
        '    Me.Panel5.Visible = False
        'End If
        If IdSistema = "LO" Then
            Me.Panel3.Enabled = False
            'NOTAS DE CREDITO DESEPARECEN PARA LOGITEL
            Me.Panel5.Enabled = False
            Me.Panel5.Visible = False
        End If
        Me.ComboBox1.Text = ""
        Me.ComboBox2.Text = ""
        Suma(0)
    End Sub

    Private Sub TarjetaTextBox_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles TarjetaTextBox.Click
        Me.TarjetaTextBox.SelectionStart = 0
        Me.TarjetaTextBox.SelectionLength = Len(Me.TarjetaTextBox.Text)
    End Sub

    Private Sub TarjetaTextBox_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles TarjetaTextBox.GotFocus
        Me.TarjetaTextBox.SelectionStart = 0
        Me.TarjetaTextBox.SelectionLength = Len(Me.TarjetaTextBox.Text)
    End Sub

    Private Sub TarjetaTextBox_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TarjetaTextBox.TextChanged
        Suma(2)
    End Sub

    Private Sub ChequeTextBox_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ChequeTextBox.Click
        Me.ChequeTextBox.SelectionStart = 0
        Me.ChequeTextBox.SelectionLength = Len(Me.ChequeTextBox.Text)
    End Sub

    Private Sub ChequeTextBox_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles ChequeTextBox.GotFocus
        Me.ChequeTextBox.SelectionStart = 0
        Me.ChequeTextBox.SelectionLength = Len(Me.ChequeTextBox.Text)
    End Sub

    Private Sub ChequeTextBox_TabIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ChequeTextBox.TabIndexChanged

    End Sub

    Private Sub ChequeTextBox_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ChequeTextBox.TextChanged
        Suma(1)
    End Sub



    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        GLOSIPAGO = 2
        Me.Close()
    End Sub

    Private Sub Button8_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button8.Click
        If IsNumeric(Me.ChequeTextBox.Text) = True Then
            If Me.ChequeTextBox.Text > 0 Then
                If Len(Trim(Me.ComboBox1.Text)) = 0 Then
                    MsgBox("Seleccione el Banco")
                    Exit Sub
                End If
                If Len(Trim(Me.TextBox1.Text)) = 0 Then
                    MsgBox("Capture el Numero del Cheque ")
                    Exit Sub
                End If
            End If
        End If
        If IsNumeric(Me.TarjetaTextBox.Text) = True Then
            If Me.TarjetaTextBox.Text > 0 Then
                If Len(Trim(Me.ComboBox2.Text)) = 0 Then
                    MsgBox("Seleccione el Banco")
                    Exit Sub
                End If
                If Len(Trim(Me.TextBox3.Text)) = 0 Then
                    MsgBox("Capture el Numero de la Tarjeta  ")
                    Exit Sub
                End If
                If Len(Trim(Me.TextBox2.Text)) = 0 Then
                    MsgBox("Capture el Numero de Autorizaci�n  ")
                    Exit Sub
                End If
                If Len(Trim(Me.CmbTipCuenta.Text)) = 0 Then
                    MsgBox("Seleccione el Tipo de Cuenta")
                    Exit Sub
                End If
            End If
        End If
        If IsNumeric(tbTransferencia.Text) = True Then
            If cbBancoTransferencia.Text.Length = 0 Then
                MessageBox.Show("Selecciona el Banco de la Transferencia Bancaria.")
                Exit Sub
            End If
            If tbNumeroTransferencia.Text.Length = 0 Then
                MessageBox.Show("Captura el N�mero de la Transferencia Bancaria.")
                Exit Sub
            End If
            If tbAutorizacionTransferencia.Text.Length = 0 Then
                MessageBox.Show("Captura la Autorizaci�n de la Transferencia Bancaria.")
                Exit Sub
            End If
        End If

        If IsNumeric(Me.CMBSaldoLabel.Text) = False Then Me.CMBSaldoLabel.Text = 0
        If Me.CMBSaldoLabel.Text = 0 Then
            GLOCLV_NOTA = 0
            GLONOTA = 0
            GLOCHEQUE = 0
            GLOCLV_BANCOCHEQUE = 0
            NUMEROCHEQUE = ""
            GLOTARJETA = 0
            GLOCLV_BANCOTARJETA = 0
            NUMEROTARJETA = ""
            TARJETAAUTORIZACION = ""
            GLOTOTALEFECTIVO = 0
            GLOCAMBIO = 0

            If IsNumeric(Me.TarjetaTextBox.Text) = False Then GLOTARJETA = 0 Else GLOTARJETA = Me.TarjetaTextBox.Text
            If IsNumeric(Me.ChequeTextBox.Text) = False Then GLOCHEQUE = 0 Else GLOCHEQUE = Me.ChequeTextBox.Text
            If IsNumeric(Me.TextBox5.Text) = False Then GLOCLV_NOTA = 0 Else GLOCLV_NOTA = Me.TextBox5.Text
            If IsNumeric(Me.TextBox4.Text) = False Then GLONOTA = 0 Else GLONOTA = Me.TextBox4.Text
            If IsNumeric(tbTransferencia.Text) = False Then GLOTRANSFERENCIA = 0 Else GLOTRANSFERENCIA = tbTransferencia.Text
            If GLONOTA = 0 Then GLOCLV_NOTA = 0
            If GLOCHEQUE > 0 Then
                GLOCHEQUE = Me.ChequeTextBox.Text
                GLOCLV_BANCOCHEQUE = Me.ComboBox1.SelectedValue
                NUMEROCHEQUE = Me.TextBox1.Text
            End If
            If GLOTARJETA > 0 Then
                GLOTARJETA = Me.TarjetaTextBox.Text
                GLOCLV_BANCOTARJETA = Me.ComboBox2.SelectedValue
                NUMEROTARJETA = Me.TextBox3.Text
                TARJETAAUTORIZACION = Me.TextBox2.Text
                GloTipoTarjeta = Me.CmbTipCuenta.SelectedIndex + 1
            End If

            If GLOTRANSFERENCIA > 0 Then
                GLOBANCO = cbBancoTransferencia.SelectedValue
                GLONUMEROTRANSFERENCIA = tbNumeroTransferencia.Text
                GLOAUTORIZACION = tbAutorizacionTransferencia.Text
            End If

            If IsNumeric(GLOTARJETA) = False Then GLOTARJETA = 0
            If IsNumeric(GLOCHEQUE) = False Then GLOCHEQUE = 0
            If IsNumeric(Me.CMBTotalaPagarLabel.Text) = False Then GLOEFECTIVO = 0 Else GLOEFECTIVO = Me.CMBTotalaPagarLabel.Text
            GLOEFECTIVO = GLOEFECTIVO - (GLOTARJETA + GLOCHEQUE + GLONOTA + GLOTRANSFERENCIA)

            If IsNumeric(EfectivoTextBox.Text) = True Then
                GLOTOTALEFECTIVO = EfectivoTextBox.Text
            Else
                GLOTOTALEFECTIVO = 0
            End If

            If IsNumeric(CMBCambioLabel.Text) = True Then
                GLOCAMBIO = CMBCambioLabel.Text
            Else
                GLOCAMBIO = 0
            End If

            GLOSIPAGO = 1


            'If locBndBon1 = True Then
            '    'Guarda el Supervisor de la Bonificacion
            '    'GloClv_Factura
            '    'LocSupBon
            '    Dim CON As New SqlConnection(MiConexion)
            '    CON.Open()
            '    MsgBox(GloClv_Factura.ToString, MsgBoxStyle.Information)
            '    If GloClv_Factura > 0 Then
            '        Me.Inserta_Bonificacion_SupervisorTableAdapter.Connection = CON
            '        Me.Inserta_Bonificacion_SupervisorTableAdapter.Fill(Me.Procedimientos_arnoldo.Inserta_Bonificacion_Supervisor, GloClv_Factura, LocSupBon)
            '        CON.Close()
            '    End If
            'End If
            Me.Close()
        Else
            MsgBox("No se ha saldado la factura", MsgBoxStyle.Information)
        End If


    End Sub



    Private Sub TextBox1_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles TextBox1.KeyPress
        e.KeyChar = Chr((ValidaKey(TextBox1, Asc(LCase(e.KeyChar)), "N")))
    End Sub



    Private Sub TextBox3_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles TextBox3.KeyPress
        e.KeyChar = Chr((ValidaKey(TextBox3, Asc(LCase(e.KeyChar)), "N")))
    End Sub


    Private Sub TextBox5_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TextBox5.TextChanged
        If IsNumeric(Me.TextBox5.Text) = True Then
            'If Me.CMBSaldoLabel.Text = 0 Then
            '    Me.TextBox5.Text = 0
            'End If
            Dim cone As New SqlClient.SqlConnection(MiConexion)
            Dim comando As SqlClient.SqlCommand
            comando = New SqlClient.SqlCommand
            cone.Open()
            If Me.TextBox5.Text > 0 Then
                With comando
                    .Connection = cone
                    .CommandText = "DameMonto_NotadeCredito "
                    .CommandType = CommandType.StoredProcedure
                    .CommandTimeout = 0
                    ' Create a SqlParameter for each parameter in the stored procedure.
                    Dim prm As New SqlParameter("@Clv_Nota", SqlDbType.BigInt)
                    Dim prm1 As New SqlParameter("@contrato", SqlDbType.BigInt)
                    Dim prm6 As New SqlParameter("@Monto", SqlDbType.Decimal)
                    prm.Direction = ParameterDirection.Input
                    prm1.Direction = ParameterDirection.Input
                    prm6.Direction = ParameterDirection.Output
                    prm.Value = Me.TextBox5.Text
                    prm1.Value = GloContrato
                    prm6.Value = 0
                    .Parameters.Add(prm)
                    .Parameters.Add(prm1)
                    .Parameters.Add(prm6)
                    Dim i As Integer = comando.ExecuteNonQuery()
                    monto = prm6.Value
                    If monto = 0 Then
                        Me.REDLabel7.Visible = False
                        'Me.REDLabel7.Text = "El Cliente " + CStr(GloContrato) + " no Tiene Generada una Nota de Cr�dito con el Folio: " + CStr(Me.TextBox5.Text)
                    Else
                        Me.REDLabel7.Visible = False
                    End If
                    Me.TextBox4.Text = Format(monto, "##,##0.00")
                End With
            End If
            cone.Close()
        Else
            Me.TextBox4.Text = ""
            Me.REDLabel7.Visible = False
        End If
    End Sub


    Private Sub TextBox4_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TextBox4.TextChanged
        Suma(3)
    End Sub


    Private Sub MUESTRABANCOS()
        BaseII.limpiaParametros()
        cbBancoTransferencia.DataSource = BaseII.ConsultaDT("MUESTRABANCOS")
        cbBancoTransferencia.Text = ""
        cbBancoTransferencia.DisplayMember = "nombre"
        cbBancoTransferencia.ValueMember = "Clave"
    End Sub

    Private Sub tbTransferencia_TextChanged(sender As System.Object, e As System.EventArgs) Handles tbTransferencia.TextChanged
        Suma(4)
    End Sub
End Class