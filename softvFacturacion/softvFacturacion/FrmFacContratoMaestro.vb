Imports CrystalDecisions.Shared
Imports CrystalDecisions.CrystalReports.Engine
Imports Microsoft.VisualBasic
Imports System.Data.SqlClient


Public Class FrmFacContratoMaestro

    Private customersByCityReport As ReportDocument
    Dim BndError As Integer = 0
    Dim Loc_Clv_Vendedor As Integer = 0
    Dim Loc_Folio As Long = 0
    Dim Loc_Serie As String = Nothing
    Dim Msg As String = Nothing
    Dim bloqueado, identi As Integer
    Dim Bnd As Integer = 0
    Dim CuantasTv As Integer = 0
    Private LocNomImpresora_Contratos As String = Nothing
    Private LocNomImpresora_Tarjetas As String = Nothing
    Dim SiPagos As Integer = 0
    Private eMsjTickets As String = Nothing
    Private eActTickets As Boolean = False
    Private eClv_Id As Integer = 0
    Private eNombreBD As String = Nothing
    Private eClave_Txt As String = Nothing
    Private eCiudad As String = Nothing

    Private Sub SetDBLogonForReport(ByVal myConnectionInfo As ConnectionInfo, ByVal myReportDocument As ReportDocument)
        customersByCityReport.DataSourceConnections(0).SetConnection(GloServerName, GloDatabaseName, GloUserID, GloPassword)
        'customersByCityReport.SetDatabaseLogon(GloUserID, GloPassword, GloServerName, GloDatabaseName)

        Dim myTables As Tables = myReportDocument.Database.Tables
        Dim myTable As CrystalDecisions.CrystalReports.Engine.Table
        For Each myTable In myTables
            Dim myTableLogonInfo As TableLogOnInfo = myTable.LogOnInfo
            myTableLogonInfo.ConnectionInfo = myConnectionInfo
            myTable.ApplyLogOnInfo(myTableLogonInfo)
            myTable.Location = GloDatabaseName & ".dbo." & myTable.Location.Substring(myTable.Location.LastIndexOf(".") + 1)
        Next
    End Sub

    Private Sub SetDBLogonForSubReport(ByVal myConnectionInfo As ConnectionInfo, ByVal myReportDocument As ReportDocument)
        customersByCityReport.Subreports(0).SetDatabaseLogon(GloUserID, GloPassword, GloServerName, GloDatabaseName)
        'customersByCityReport.Subreports(0).DataSourceConnections(0).SetConnection(GloServerName, GloDatabaseName, GloUserID, GloPassword)

        Dim I As Integer = myReportDocument.Subreports.Count
        Dim X As Integer = 0
        For X = 0 To I - 1
            Dim myTables As Tables = myReportDocument.Subreports(X).Database.Tables
            Dim myTable As CrystalDecisions.CrystalReports.Engine.Table
            For Each myTable In myTables
                Dim myTableLogonInfo As TableLogOnInfo = myTable.LogOnInfo
                myTableLogonInfo.ConnectionInfo = myConnectionInfo
                myTable.ApplyLogOnInfo(myTableLogonInfo)
                myTable.Location = GloDatabaseName & ".dbo." & myTable.Location.Substring(myTable.Location.LastIndexOf(".") + 1)
            Next
        Next X
    End Sub


    Private Sub CREAARBOL()

        Try
            Dim I As Integer = 0
            Dim X As Integer = 0
            Dim Y As Integer = 0
            Dim epasa As Boolean = True
            ' Assumes that customerConnection is a valid SqlConnection object.
            ' Assumes that orderConnection is a valid OleDbConnection object.
            'Dim custAdapter As SqlClient.SqlDataAdapter = New SqlClient.SqlDataAdapter( _
            '  "SELECT * FROM dbo.Customers", customerConnection)''

            'Dim customerOrders As DataSet = New DataSet()
            'custAdapter.Fill(customerOrders, "Customers")
            ' 
            'Dim pRow, cRow As DataRow
            'For Each pRow In customerOrders.Tables("Customers").Rows
            ' msgbox(pRow("CustomerID").ToString())
            'Next
            Dim CON As New SqlConnection(MiConexion)
            CON.Open()
            If IsNumeric(Me.ContratoTextBox.Text) = True Then
                Me.DameSerDELCliFACTableAdapter.Connection = CON
                Me.DameSerDELCliFACTableAdapter.Fill(Me.NewsoftvDataSet.DameSerDELCliFAC, New System.Nullable(Of Long)(CType(Me.ContratoTextBox.Text, Long)))
            Else
                Me.DameSerDELCliFACTableAdapter.Connection = CON
                Me.DameSerDELCliFACTableAdapter.Fill(Me.NewsoftvDataSet.DameSerDELCliFAC, New System.Nullable(Of Long)(CType(0, Long)))
            End If
            CON.Close()
            Dim pasa As Boolean = False
            Dim Net As Boolean = False
            Dim dig As Boolean = False
            Dim jNet As Integer = -1
            Dim PasaJNet As Boolean = False
            Dim jDig As Integer = -1
            Dim FilaRow As DataRow
            'Me.TextBox1.Text = ""
            Me.TreeView1.Nodes.Clear()
            For Each FilaRow In Me.NewsoftvDataSet.DameSerDELCliFAC.Rows

                'MsgBox(Trim(FilaRow(1).ToString()) & " " & Trim(FilaRow(0).ToString()))
                X = 0
                'If Len(Trim(Me.TextBox1.Text)) = 0 Then
                'Me.TextBox1.Text = Trim(FilaRow("Servicio").ToString())
                'Else
                'Me.TextBox1.Text = Me.TextBox1.Text & " , " & Trim(FilaRow("Servicio").ToString())
                'End If
                'MsgBox(Mid(FilaRow("Servicio").ToString(), 1, 19))
                If Mid(FilaRow("Servicio").ToString(), 1, 3) = "---" Then
                    Me.TreeView1.Nodes.Add(FilaRow("Servicio").ToString()).ForeColor = System.Drawing.Color.FromArgb(ColorBwr)
                    pasa = True
                    Net = False
                    dig = False
                ElseIf Mid(FilaRow("Servicio").ToString(), 1, 15) = "Servicio Basico" Then
                    Me.TreeView1.Nodes.Add(FilaRow("Servicio").ToString()).ForeColor = System.Drawing.Color.FromArgb(ColorBwr)
                    pasa = True
                ElseIf Mid(FilaRow("Servicio").ToString(), 1, 31) = "Servicios de Televisi�n Digital" Then
                    Me.TreeView1.Nodes.Add(FilaRow("Servicio").ToString()).ForeColor = System.Drawing.Color.FromArgb(ColorBwr)
                    pasa = True
                ElseIf Mid(FilaRow("Servicio").ToString(), 1, 21) = "Servicios de Internet" Then
                    Me.TreeView1.Nodes.Add(FilaRow("Servicio").ToString()).ForeColor = System.Drawing.Color.FromArgb(ColorBwr)

                    pasa = True
                ElseIf Mid(FilaRow("Servicio").ToString(), 1, 22) = "Servicios de Tel�fonia" Then
                    Me.TreeView1.Nodes.Add(FilaRow("Servicio").ToString()).ForeColor = System.Drawing.Color.FromArgb(ColorBwr)

                    pasa = True
                Else
                    If Mid(FilaRow("Servicio").ToString(), 1, 14) = "Mac Cablemodem" Then
                        Me.TreeView1.Nodes(I - 1).Nodes.Add(FilaRow("Servicio").ToString()).ForeColor = Color.Black
                        jNet = jNet + 1
                        pasa = False
                        Net = True
                    ElseIf Mid(FilaRow("Servicio").ToString(), 1, 15) = "Aparato Digital" Then
                        Me.TreeView1.Nodes(I - 1).Nodes.Add(FilaRow("Servicio").ToString()).ForeColor = Color.Black
                        jDig = jDig + 1
                        pasa = False
                        dig = True
                    Else
                        If Net = True Then
                            Me.TreeView1.Nodes(I - 1).Nodes(jNet).Nodes.Add(FilaRow("Servicio").ToString()).ForeColor = Color.Gray
                        ElseIf dig = True Then
                            Me.TreeView1.Nodes(I - 1).Nodes(jDig).Nodes.Add(FilaRow("Servicio").ToString()).ForeColor = Color.Gray
                        Else
                            If epasa = True Then
                                Me.TreeView1.Nodes(I - 1).Nodes.Add(FilaRow("Servicio").ToString()).ForeColor = Color.Black
                                pasa = False
                                epasa = False
                            Else
                                Me.TreeView1.Nodes(I - 1).Nodes.Add(FilaRow("Servicio").ToString()).ForeColor = Color.Gray
                                epasa = False
                                pasa = False
                            End If

                        End If
                    End If
                End If
                If pasa = True Then I = I + 1
            Next
            'Me.TreeView1.Nodes(0).ExpandAll()
            For Y = 0 To (I - 1)
                Me.TreeView1.Nodes(Y).ExpandAll()
            Next
        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try

    End Sub

    Private Sub FrmFacContratoMaestro_Activated(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Activated
        If Glocontratosel > 0 Then
            Me.ContratoTextBox.Text = 0
            Me.ContratoTextBox.Text = Glocontratosel
            Glocontratosel = 0
        End If
    End Sub

    Private Sub FrmFacContratoMaestro_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        If IsNumeric(Me.Clv_Cobro.Text) = True Then
            If Me.Clv_Cobro.Text > 0 Then
                Dim CON As New SqlConnection(MiConexion)
                CON.Open()
                Me.Borra_Clv_ContratoMaestroTableAdapter.Connection = CON
                Me.Borra_Clv_ContratoMaestroTableAdapter.Fill(Me.DataSetEdgar.Borra_Clv_ContratoMaestro, New System.Nullable(Of Long)(CType(Me.Clv_Cobro.Text, Long)))
                CON.Close()
            End If
        End If
    End Sub

    Private Sub FrmFacContratoMaestro_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        colorea(Me)
        GloTipo = "C"
        GloOpFacturas = 3
        colorea(Me)
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        Me.DamedatosUsuarioTableAdapter.Connection = CON
        Me.DamedatosUsuarioTableAdapter.Fill(Me.NewsoftvDataSet.DamedatosUsuario, GloUsuario)
        Me.DAMENOMBRESUCURSALTableAdapter.Connection = CON
        Me.DAMENOMBRESUCURSALTableAdapter.Fill(Me.NewsoftvDataSet.DAMENOMBRESUCURSAL, GloSucursal)
        Me.DameDatosGeneralesTableAdapter.Connection = CON
        Me.DameDatosGeneralesTableAdapter.Fill(Me.NewsoftvDataSet.DameDatosGenerales)

        'Me.ConGeneralRelBDTableAdapter.Connection = CON
        'Me.ConGeneralRelBDTableAdapter.Fill(Me.DataSetEdgar.ConGeneralRelBD)
        Me.ConGeneralRelBD1TableAdapter.Connection = CON
        Me.ConGeneralRelBD1TableAdapter.Fill(Me.DataSetEdgar.ConGeneralRelBD1, eClv_Id, eNombreBD, eClave_Txt, eCiudad)
        CON.Close()
        Me.LblNomCaja.Text = GlonOMCaja
        Me.LblVersion.Text = My.Application.Info.Version.ToString
    End Sub

    Private Sub Busca_Datos_del_Cliente()
        Try
            GloContrato = 0
            If IsNumeric(Me.ContratoTextBox.Text) = True Then
                If Me.ContratoTextBox.Text > 0 Then
                    GloContrato = Me.ContratoTextBox.Text
                    Dim CON As New SqlConnection(MiConexion)
                    CON.Open()
                    Me.BUSCLIPORCONTRATO_FACTableAdapter.Connection = CON
                    Me.BUSCLIPORCONTRATO_FACTableAdapter.Fill(Me.NewsoftvDataSet.BUSCLIPORCONTRATO_FAC, Me.ContratoTextBox.Text, 99)
                    CON.Close()

                    CREAARBOL()
                    DAMETIPOSCLIENTEDAME()
                    '--Dim Clv_Cobro As Object = Clv_CobroToolStripTextBox.Text
                    If IsNumeric(Me.Clv_Cobro.Text) = True Then
                        If Me.Clv_Cobro.Text > 0 Then
                            Dim CON2 As New SqlConnection(MiConexion)
                            CON2.Open()
                            Me.Borra_Clv_ContratoMaestroTableAdapter.Connection = CON2
                            Me.Borra_Clv_ContratoMaestroTableAdapter.Fill(Me.DataSetEdgar.Borra_Clv_ContratoMaestro, New System.Nullable(Of Long)(CType(Me.Clv_Cobro.Text, Long)))
                            CON2.Close()
                        End If
                    End If
                    Dim CON3 As New SqlConnection(MiConexion)
                    CON3.Open()
                    Me.Cobra_MaestroTableAdapter.Connection = con3
                    Me.Cobra_MaestroTableAdapter.Fill(Me.DataSetEdgar.Cobra_Maestro, New System.Nullable(Of Long)(CType(Me.ContratoTextBox.Text, Long)), Clv_Cobro.Text)
                    Me.Dame_Cobro_MaestroTableAdapter.Connection = con3
                    Me.Dame_Cobro_MaestroTableAdapter.Fill(Me.DataSetEdgar.Dame_Cobro_Maestro, New System.Nullable(Of Long)(CType(Clv_Cobro.Text, Long)))
                    Me.Dame_Total_Cobro_MaestroTableAdapter.Connection = con3
                    Me.Dame_Total_Cobro_MaestroTableAdapter.Fill(Me.DataSetEdgar.Dame_Total_Cobro_Maestro, New System.Nullable(Of Long)(CType(Me.Clv_Cobro.Text, Long)))
                    Me.BUSCLIPORCONTRATO_FACTableAdapter.Connection = con3
                    Me.BUSCLIPORCONTRATO_FACTableAdapter.Fill(Me.NewsoftvDataSet.BUSCLIPORCONTRATO_FAC, Me.ContratoTextBox.Text, 99)
                    con3.close()
                    CREAARBOL()
                    DAMETIPOSCLIENTEDAME()
                Else
                    Dim CON4 As New SqlConnection(MiConexion)
                    CON4.Open()
                    Me.Dame_Cobro_MaestroTableAdapter.Connection = con4
                    Me.Dame_Cobro_MaestroTableAdapter.Fill(Me.DataSetEdgar.Dame_Cobro_Maestro, New System.Nullable(Of Long)(CType(0, Long)))
                    Me.Dame_Total_Cobro_MaestroTableAdapter.Connection = con4
                    Me.Dame_Total_Cobro_MaestroTableAdapter.Fill(Me.DataSetEdgar.Dame_Total_Cobro_Maestro, New System.Nullable(Of Long)(CType(0, Long)))
                    Me.BUSCLIPORCONTRATO_FACTableAdapter.Connection = con4
                    Me.BUSCLIPORCONTRATO_FACTableAdapter.Fill(Me.NewsoftvDataSet.BUSCLIPORCONTRATO_FAC, 0, 99)
                    con4.close()
                    CREAARBOL()
                    DAMETIPOSCLIENTEDAME()
                End If
            Else
                Dim CON5 As New SqlConnection(MiConexion)
                CON5.Open()
                Me.Dame_Cobro_MaestroTableAdapter.Connection = con5
                Me.Dame_Cobro_MaestroTableAdapter.Fill(Me.DataSetEdgar.Dame_Cobro_Maestro, New System.Nullable(Of Long)(CType(0, Long)))
                Me.Dame_Total_Cobro_MaestroTableAdapter.connection = con5
                Me.Dame_Total_Cobro_MaestroTableAdapter.Fill(Me.DataSetEdgar.Dame_Total_Cobro_Maestro, New System.Nullable(Of Long)(CType(0, Long)))
                Me.BUSCLIPORCONTRATO_FACTableAdapter.Connection = con5
                Me.BUSCLIPORCONTRATO_FACTableAdapter.Fill(Me.NewsoftvDataSet.BUSCLIPORCONTRATO_FAC, 0, 99)
                CON5.Close()
                CREAARBOL()
                DAMETIPOSCLIENTEDAME()
            End If
        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub ContratoTextBox_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ContratoTextBox.TextChanged
        Busca_Datos_del_Cliente()
    End Sub

    Private Sub DAMETIPOSCLIENTEDAME()
        Try
            Dim CON As New SqlConnection(MiConexion)
            CON.Open()
            Me.CLV_TIPOCLIENTELabel1.Text = ""
            Me.DESCRIPCIONLabel1.Text = ""
            Me.DAMETIPOSCLIENTESTableAdapter.Connection = CON
            Me.DAMETIPOSCLIENTESTableAdapter.Fill(Me.DataSetEdgar.DAMETIPOSCLIENTES, 0)
            If IsNumeric(Me.ContratoTextBox.Text) = True Then
                If Me.ContratoTextBox.Text > 0 Then
                    Me.DAMETIPOSCLIENTESTableAdapter.Connection = CON
                    Me.DAMETIPOSCLIENTESTableAdapter.Fill(Me.DataSetEdgar.DAMETIPOSCLIENTES, Me.ContratoTextBox.Text)
                End If
            End If
            CON.Close()
        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub FillToolStripButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)

    End Sub

    Private Sub Dame_Cobro_MaestroBindingNavigatorSaveItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Dame_Cobro_MaestroBindingNavigatorSaveItem.Click
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        Me.Validate()
        Me.Dame_Cobro_MaestroBindingSource.EndEdit()
        Me.Dame_Cobro_MaestroTableAdapter.Connection = CON
        Me.Dame_Cobro_MaestroTableAdapter.Update(Me.DataSetEdgar.Dame_Cobro_Maestro)
        If IsNumeric(Me.Clv_Cobro.Text) = True Then
            Me.Dame_Total_Cobro_MaestroTableAdapter.Connection = CON
            Me.Dame_Total_Cobro_MaestroTableAdapter.Fill(Me.DataSetEdgar.Dame_Total_Cobro_Maestro, New System.Nullable(Of Long)(CType(Me.Clv_Cobro.Text, Long)))
        End If
        CON.Close()
    End Sub




    Private Sub ToolStripButton1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ToolStripButton1.Click
        Me.Dame_Cobro_MaestroBindingSource.CancelEdit()
    End Sub





    Private Sub Importe_TotalTextBox_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Importe_TotalTextBox.TextChanged
        If IsNumeric(Me.Importe_TotalTextBox.Text) = True Then
            Me.Label14.Text = Format(CDbl(Me.Importe_TotalTextBox.Text), "##,##0.00")
        Else
            Me.Label14.Text = 0
        End If
    End Sub

    Private Sub Label14_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Label14.Click

    End Sub

    Private Sub ConfigureCrystalReports(ByVal Clv_Factura As Long)
        Try


            Dim ba As Boolean = False
            customersByCityReport = New ReportDocument
            Dim connectionInfo As New ConnectionInfo
            '"Data Source=SERVER-2003\SQLEXPRESS;Initial Catalog=Newsoftv;Persist Security Info"& _ 
            '    "=True;User ID=DeSistema;Password=1975huli")
            connectionInfo.ServerName = GloServerName
            connectionInfo.DatabaseName = GloDatabaseName
            connectionInfo.UserID = GloUserID
            connectionInfo.Password = GloPassword

            Dim reportPath As String = Nothing

            Dim CON As New SqlConnection(MiConexion)
            CON.Open()
            Me.BusFacFiscalTableAdapter.Connection = CON
            Me.BusFacFiscalTableAdapter.Fill(Me.NewsoftvDataSet2.BusFacFiscal, Clv_Factura, identi)
            CON.Close()

            eActTickets = False
            Dim CON2 As New SqlConnection(MiConexion)
            CON2.Open()
            Me.DameGeneralMsjTicketsTableAdapter.Connection = CON2
            Me.DameGeneralMsjTicketsTableAdapter.Fill(Me.EricDataSet2.DameGeneralMsjTickets, eMsjTickets, eActTickets)
            CON2.Close()

            If IdSistema = "SA" And facnormal = True And identi > 0 Then
                reportPath = RutaReportes + "\ReporteCajasTvRey.rpt"
                ba = True
            ElseIf IdSistema = "TO" And facnormal = True And identi > 0 Then
                reportPath = RutaReportes + "\ReporteCajasCabSta.rpt"
                ba = True
            ElseIf IdSistema = "AG" And facnormal = True And identi > 0 Then
                reportPath = RutaReportes + "\ReporteCajasGiga.rpt"
                ba = True
            ElseIf IdSistema = "VA" And facnormal = True And identi > 0 Then
                reportPath = RutaReportes + "\ReporteCajasGiga.rpt"
                ba = True
            Else
                If IdSistema = "VA" Then
                    'reportPath = RutaReportes + "\ReporteCajasTicketsCosmo.rpt"
                Else
                    reportPath = RutaReportes + "\ReporteCajasTickets_2.rpt"
                End If
            End If



            customersByCityReport.Load(reportPath)
            SetDBLogonForReport(connectionInfo, customersByCityReport)


            '@Clv_Factura 
            customersByCityReport.SetParameterValue(0, Clv_Factura)
            '@Clv_Factura_Ini
            customersByCityReport.SetParameterValue(1, "0")
            '@Clv_Factura_Fin
            customersByCityReport.SetParameterValue(2, "0")
            '@Fecha_Ini
            customersByCityReport.SetParameterValue(3, "01/01/1900")
            '@Fecha_Fin
            customersByCityReport.SetParameterValue(4, "01/01/1900")
            '@op
            customersByCityReport.SetParameterValue(5, "0")

            If ba = False Then
                customersByCityReport.DataDefinition.FormulaFields("Empresa").Text = "'" & GloEmpresa & "'"
                customersByCityReport.DataDefinition.FormulaFields("DireccionEmpresa").Text = "'" & GloDireccionEmpresa & "'"
                customersByCityReport.DataDefinition.FormulaFields("Colonia_CpEmpresa").Text = "'" & GloColonia_CpEmpresa & "'"
                customersByCityReport.DataDefinition.FormulaFields("CiudadEmpresa").Text = "'" & GloCiudadEmpresa & "'"
                customersByCityReport.DataDefinition.FormulaFields("RfcEmpresa").Text = "'" & GloRfcEmpresa & "'"
                customersByCityReport.DataDefinition.FormulaFields("TelefonoEmpresa").Text = "'" & GloTelefonoEmpresa & "'"
                If eActTickets = True Then
                    customersByCityReport.DataDefinition.FormulaFields("Mensaje").Text = "'" & eMsjTickets & "'"
                End If
            End If


            'If (IdSistema = "TO" Or IdSistema = "SA" Or IdSistema = "AG" Or IdSistema = "VA") And facnormal = True And identi > 0 Then
            '    customersByCityReport.PrintOptions.PrinterName = impresorafiscal
            'Else

            customersByCityReport.PrintOptions.PrinterName = LocImpresoraTickets
            'End If



            customersByCityReport.PrintToPrinter(1, True, 1, 1)


            customersByCityReport = Nothing
        Catch ex As Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        Dim CON2 As New SqlConnection(MiConexion)
        Dim Bnd As Boolean = False
        Dim i As Integer = 0
        Dim cont As Integer
        Dim Rfc As String = Nothing
        Dim filas As Integer

        CON2.Open()
        Me.Dame_clv_session_ReportesTableAdapter.Connection = CON2
        Me.Dame_clv_session_ReportesTableAdapter.Fill(Me.DataSetEdgar.Dame_clv_session_Reportes, eClv_Session)
        CON2.Close()
        'Me.GrabaFacturasTableAdapter.Fill(Me.NewsoftvDataSet.GrabaFacturas, Me.ContratoTextBox.Text, Me.Clv_Session.Text, GloUsuario, GloSucursal, GloCaja, GloTipo, Loc_Serie, Loc_Folio, Loc_Clv_Vendedor, BndError, Msg, GloClv_Factura)
        'Me.GUARDATIPOPAGOTableAdapter.Fill(Me.NewsoftvDataSet1.GUARDATIPOPAGO, GloClv_Factura, GLOEFECTIVO, GLOCHEQUE, GLOCLV_BANCOCHEQUE, NUMEROCHEQUE, GLOTARJETA, GLOCLV_BANCOTARJETA, NUMEROTARJETA, TARJETAAUTORIZACION)
        If Me.Dame_Cobro_MaestroDataGridView.RowCount > 0 Then
            Dim resp As MsgBoxResult = MsgBoxResult.No
            resp = MsgBox(" � Deseas realizar el cobro ?", MsgBoxStyle.YesNo)
            If resp = MsgBoxResult.Yes Then
                Dim facturas() As Integer = {0}
                Dim FilaRow As DataRow
                filas = Me.DataSetEdgar.Dame_Cobro_Maestro.Rows.Count

                For Each FilaRow In Me.DataSetEdgar.Dame_Cobro_Maestro.Rows
                    'MsgBox(FilaRow("ErrorSub").ToString())
                    If FilaRow("ErrorSub").ToString() = 0 And FilaRow("Realizar_Cobro").ToString() = True Then
                        'MsgBox(FilaRow("Clv_Session").ToString() + " " + FilaRow("Clv_Session").ToString())
                        Dim CON As New SqlConnection(MiConexion)
                        CON.Open()
                        Me.GrabaFacturasTableAdapter.Connection = CON
                        Me.GrabaFacturasTableAdapter.Fill(Me.NewsoftvDataSet.GrabaFacturas, FilaRow("contrato").ToString(), FilaRow("Clv_Session").ToString(), GloUsuario, GloSucursal, GloCaja, "C", "", 0, 0, BndError, Msg, GloClv_Factura)
                        Me.GUARDATIPOPAGOTableAdapter.Connection = CON
                        Me.GUARDATIPOPAGOTableAdapter.Fill(Me.NewsoftvDataSet1.GUARDATIPOPAGO, GloClv_Factura, FilaRow("total").ToString(), 0, 0, "", 0, 0, "", "")
                        Me.NueRelContratoMaestroFacTableAdapter.Connection = CON
                        Me.NueRelContratoMaestroFacTableAdapter.Fill(Me.DataSetEdgar.NueRelContratoMaestroFac, Me.ContratoTextBox.Text, FilaRow("contrato").ToString(), GloClv_Factura, eClv_Session)
                        CON.Close()

                        bitsist(GloUsuario, Me.ContratoTextBox.Text, GloSistema, Me.Text, "Se Cobro Un Contrato Maestro", "", "factura:" + CStr(GloClv_Factura), SubCiudad)

                        If LocImpresoraTickets = "" Then
                            MsgBox("No Se Ha Asignado Una Impresora De Tickets A Esta Sucursal", MsgBoxStyle.Information)
                        Else
                            If IdSistema = "TO" Or IdSistema = "AG" Or eClave_Txt = "TV" Or eClave_Txt = "VA" Then
                                ConfigureCrystalReports(GloClv_Factura)

                            End If

                            'facturas(i) = GloClv_Factura
                            'CON.Open()
                            'Me.Dame_Si_Datos_FiscalesTableAdapter.Connection = CON
                            'Me.Dame_Si_Datos_FiscalesTableAdapter.Fill(Me.NewsoftvDataSet2.Dame_Si_Datos_Fiscales, GloClv_Factura, cont, Rfc)

                            'If (cont = 3 Or (cont > 0 And i = 0)) And Unico = True Then
                            '    Unico = True
                            'ElseIf Unico = False Or cont = 0 Then
                            '    Unico = False
                            'End If

                        End If
                        Bnd = True
                        i = i + 1
                    ElseIf FilaRow("ErrorSub").ToString() <> 0 Then

                    End If
                Next
                If Bnd = True Then
                    'If Unico = False Then
                    '    For j As Integer = 0 To filas
                    '        ConfigureCrystalReports(facturas(j))
                    '    Next
                    'ElseIf Unico = True Then
                    '    Factura_inicial = facturas(0)
                    '    Factura_final = (filas)
                    '    ConfigureCrystalReports(facturas(0))

                    'End If
                    Bnd = False

                    'If IdSistema = "SA" Then
                    '    ConfigureCrystalReportsMaestro(eClv_Session)
                    'End If

                    'identi = 0
                    'identi = BusFacFiscalOledbMaestro(eClv_Session)

                    'locID_Compania_Mizart = ""
                    'locID_Sucursal_Mizart = ""

                    'If CInt(identi) > 0 Then
                    '    Locop = 0
                    '    Dime_Aque_Compania_FacturarleMaestro(eClv_Session)
                    '    Graba_Factura_DigitalMaestro(eClv_Session, identi)

                    '    Try
                    '        FormPruebaDigital.Show()
                    '    Catch ex As Exception

                    '    End Try
                    '    locID_Compania_Mizart = ""
                    '    locID_Sucursal_Mizart = ""

                    'Else
                    '    If LocImpresoraTickets = "" Then
                    '        MsgBox("No Se Ha Asignado Una Impresora de Tickets A Esta Sucursal", MsgBoxStyle.Information)
                    '        GLOSIPAGO = 0
                    '        Me.ContratoTextBox.Text = 0
                    '        GloContrato = 0
                    '        Glocontratosel = 0
                    '        Me.Clv_Session.Text = 0
                    '        BUSCACLIENTES(0)
                    '    Else
                    '        ConfigureCrystalReports(GloClv_Factura)
                    '    End If
                    'End If

                    If GloActivarCFD = 1 Then
                        Try
                            HazFacturaDigitalMaestro(eClv_Session)
                        Catch ex As Exception
                            MsgBox(ex.Message)
                        End Try

                    End If

                    MsgBox("Se Grabo Con Ex�to ", MsgBoxStyle.Information)

                Else
                    MsgBox("Seleccione el contrato que desea cobrar")
                End If
                Me.ContratoTextBox.Text = "0"
            End If
        End If
    End Sub

    Private Sub HazFacturaDigitalMaestro(ByVal Clv_Session As Long)
        Try
            FacturacionDigitalSoftv.ClassCFDI.MiConexion = MiConexion
            Dim identi As Integer = 0
            FacturacionDigitalSoftv.ClassCFDI.EsTimbrePrueba = False
            identi = 0
            identi = FacturacionDigitalSoftv.ClassCFDI.BusFacFiscalOledbMaestro(Clv_Session, MiConexion)
            'fin Facturacion Digital
            FacturacionDigitalSoftv.ClassCFDI.locID_Compania_Mizart = ""
            FacturacionDigitalSoftv.ClassCFDI.locID_Sucursal_Mizart = ""

            If CInt(identi) > 0 Then
                FacturacionDigitalSoftv.ClassCFDI.Locop = 0
                FacturacionDigitalSoftv.ClassCFDI.Dime_Aque_Compania_FacturarleMaestro(Clv_Session, MiConexion)
                FacturacionDigitalSoftv.ClassCFDI.Graba_Factura_DigitalMaestro(Clv_Session, identi, MiConexion)
                Try
                    If FacturacionDigitalSoftv.ClassCFDI.GloClv_FacturaCFD > 0 Then
                        Dim frm As New FacturacionDigitalSoftv.FrmImprimir
                        frm.ShowDialog()
                    Else
                        MsgBox("No se genero la factura digital cancele y vuelva a intentar por favor")
                    End If
                    'FormPruebaDigital.Show()
                Catch ex As Exception
                End Try

                FacturacionDigitalSoftv.ClassCFDI.locID_Compania_Mizart = ""
                FacturacionDigitalSoftv.ClassCFDI.locID_Sucursal_Mizart = ""

            End If
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try

        Dim r As New Globalization.CultureInfo("es-MX")
        r.DateTimeFormat.ShortDatePattern = "dd/MM/yyyy"
        System.Threading.Thread.CurrentThread.CurrentCulture = r
    End Sub

    Private Sub Button3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button3.Click
        Me.Close()
    End Sub

    Private Sub Button4_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button4.Click
        Me.ContratoTextBox.Text = "0"
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        Glocontratosel = 0
        eBotonGuardar = False
        FrmSelClienteMaestro.Show()
    End Sub

    Private Sub Button8_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button8.Click
        If IsNumeric(Me.ContratoTextBox.Text) = False Then GloContrato = 0 Else GloContrato = Me.ContratoTextBox.Text
        If IsNumeric(GloContrato) = True And GloContrato > 0 Then
            GloOpFacturas = 3
            eBotonGuardar = False
            BrwFacturas_Cancelar.Show()
        Else
            MsgBox("Seleccione un Cliente por favor", MsgBoxStyle.Information)
        End If
    End Sub

    Private Sub Dame_Cobro_MaestroDataGridView_CellContentClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles Dame_Cobro_MaestroDataGridView.CellContentClick
        'If e.ColumnIndex = 7 Then
        '    If SiPagos = 1 Then
        '        MsgBox("No se puede adelantar pagos con la promoci�n que se le esta aplicando")
        '        Exit Sub
        '    End If
        '    If DataGridView1.SelectedCells(7).Value = "Adelantar Pagos" Then
        '        If IsNumeric(DataGridView1.SelectedCells(0).Value) = True And IsNumeric(DataGridView1.SelectedCells(1).Value) = True And IsNumeric(DataGridView1.SelectedCells(2).Value) = True And IsNumeric(Me.ContratoTextBox.Text) = True Then
        '            gloClv_Session = DataGridView1.SelectedCells(0).Value
        '            gloClv_Servicio = DataGridView1.SelectedCells(1).Value
        '            gloClv_llave = DataGridView1.SelectedCells(2).Value
        '            gloClv_UnicaNet = DataGridView1.SelectedCells(3).Value
        '            gloClave = DataGridView1.SelectedCells(4).Value
        '            Dim ERROR_1 As Integer = 0
        '            Dim MSGERROR_1 As String = nothing
        '            Me.Pregunta_Si_Puedo_AdelantarTableAdapter.Fill(Me.DataSetEdgar.Pregunta_Si_Puedo_Adelantar, New System.Nullable(Of Long)(CType(gloClv_Session, Long)), ERROR_1, MSGERROR_1)
        '            If ERROR_1 = 0 Then
        '                My.Forms.FrmPagosAdelantados.Show()
        '            ElseIf ERROR_1 = 2 Then
        '                MsgBox(MSGERROR_1)
        '            End If
        '        ElseIf DataGridView1.SelectedCells(7).Value = "Ext. Adicionales" Then
        '            GloClv_Txt = "CEXTV"
        '            My.Forms.FrmExtecionesTv.Show()
        '        End If
        '    End If
        'End If
        If Me.Dame_Cobro_MaestroDataGridView.RowCount > 0 Then
            If e.ColumnIndex = 4 Then
                If Me.Dame_Cobro_MaestroDataGridView.SelectedCells(4).Value = "Ver Detalle" Then
                    Glo_Clv_SessionVer = Me.Dame_Cobro_MaestroDataGridView.SelectedCells(7).Value
                    Glocontratosel2 = Me.Dame_Cobro_MaestroDataGridView.SelectedCells(0).Value
                    Glo_BndErrorVer = Me.Dame_Cobro_MaestroDataGridView.SelectedCells(9).Value
                    Glo_MsgVer = Me.Dame_Cobro_MaestroDataGridView.SelectedCells(5).Value
                    FrmVerDetalleCobro.Show()
                End If
            ElseIf e.ColumnIndex = 2 Then
                Dim CON2 As New SqlConnection(MiConexion)
                CON2.Open()
                Me.Validate()
                Me.Dame_Cobro_MaestroBindingSource.EndEdit()
                Me.Dame_Cobro_MaestroTableAdapter.Connection = CON2
                Me.Dame_Cobro_MaestroTableAdapter.Update(Me.DataSetEdgar.Dame_Cobro_Maestro)
                If IsNumeric(Me.Clv_Cobro.Text) = True Then
                    Me.Dame_Total_Cobro_MaestroTableAdapter.Connection = CON2
                    Me.Dame_Total_Cobro_MaestroTableAdapter.Fill(Me.DataSetEdgar.Dame_Total_Cobro_Maestro, New System.Nullable(Of Long)(CType(Me.Clv_Cobro.Text, Long)))
                End If
                CON2.Close()
            End If
        End If
    End Sub



    Private Sub ConfigureCrystalReportsMaestro(ByVal Clv_Session As Long)
        Try


            Dim ba As Boolean = False
            customersByCityReport = New ReportDocument
            Dim connectionInfo As New ConnectionInfo
            '"Data Source=SERVER-2003\SQLEXPRESS;Initial Catalog=Newsoftv;Persist Security Info"& _ 
            '    "=True;User ID=DeSistema;Password=1975huli")
            connectionInfo.ServerName = GloServerName
            connectionInfo.DatabaseName = GloDatabaseName
            connectionInfo.UserID = GloUserID
            connectionInfo.Password = GloPassword

            Dim reportPath As String = Nothing

            reportPath = RutaReportes + "\ReporteCajasTVReyMaestro.rpt"




            customersByCityReport.Load(reportPath)

            SetDBLogonForReport(connectionInfo, customersByCityReport)


            '@Clv_Factura 
            customersByCityReport.SetParameterValue(0, eClv_Session)


            customersByCityReport.PrintOptions.PrinterName = impresorafiscal
            customersByCityReport.PrintToPrinter(1, True, 1, 1)


            customersByCityReport = Nothing
        Catch ex As Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub

    
    
    
End Class