Imports System.Data.SqlClient
Imports System.Drawing.Drawing2D
Imports System.Text
Imports System.Collections.Generic

Public Class FormCAMDO
    Private opcionlocal As String = "N"

    Private Sub FormCAMDO_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        colorea(Me)
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        'TODO: esta l�nea de c�digo carga datos en la tabla 'NewSofTvDataSet.MUESTRACALLES' Puede moverla o quitarla seg�n sea necesario.
        Me.MUESTRACALLESTableAdapter.Connection = CON
        Me.MUESTRACALLESTableAdapter.Fill(Me.NewsoftvDataSet1.MUESTRACALLES)
        CON.Close()
        Me.Clv_ColoniaComboBox.Enabled = False
        Me.Clv_CiudadComboBox.Enabled = False

        opcionlocal = "N"
        'Me.CONCAMDOFACBindingSource.AddNew()
        Me.CONTRATOTextBox.Text = GloContrato
        Me.Clv_OrdenTextBox.Text = gloClv_Session
        'Me.ContratoTextBox1.Text = GloContrato
        'Me.Clv_SessionTextBox.Text = gloClv_Session
        Busca()

        'If ContratoCAMDO > 0 Then
        '    Dim SQL As String
        '    Dim MC As New SqlConnection(MiConexion)
        '    Dim Rs As SqlDataReader
        '    Dim Com As New SqlCommand
        '    SQL = "SELECT C.Nombre,CI.Nombre,CF.sector FROM CAMDOFAC CF INNER JOIN COLONIAS C ON CF.Clv_Colonia=C.clv_colonia INNER JOIN CIUDADES CI ON CF.Clv_Ciudad=CI.Clv_Ciudad WHERE CONTRATO = " + ContratoCAMDO + " And Clv_Sesion = " + Glo_Clv_SessionVer + ""
        '    Com = New SqlCommand(SQL, MC)
        '    Rs = Com.ExecuteReader()
        '    Rs.Read()
        '    Clv_ColoniaComboBox.Text = Rs(0) 'aca me pone el primer campo del select
        '    Clv_CiudadComboBox.Text = Rs(1)
        '    SectorComboBox.Text = Rs(2)
        'End If
    End Sub

    Private Sub CONCAMDOFACBindingNavigatorSaveItem_Click_1(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CONCAMDOFACBindingNavigatorSaveItem.Click

        If IsNumeric(Me.Clv_CalleComboBox.SelectedValue) = False Then
            MsgBox("Seleccione la Calle por favor", MsgBoxStyle.Information)
            Exit Sub
        End If
        If IsNumeric(Me.Clv_ColoniaComboBox.SelectedValue) = False Then
            MsgBox("Seleccione la Colonia por favor", MsgBoxStyle.Information)
            Exit Sub
        End If
        If IsNumeric(Me.Clv_CiudadComboBox.SelectedValue) = False Or Len(Me.Clv_CiudadComboBox.Text) = 0 Then
            MsgBox("Seleccione la Ciudad por favor", MsgBoxStyle.Information)
            Exit Sub
        End If
        If Len(Trim(Me.NUMEROTextBox.Text)) = 0 Then
            MsgBox("Capture el Numero por favor", MsgBoxStyle.Information)
            Exit Sub
        End If
        If Len(Trim(Me.ENTRECALLESTextBox.Text)) = 0 Then
            MsgBox("Capture las Entrecalles por favor", MsgBoxStyle.Information)
            Exit Sub
        End If
        If Len(Trim(Me.TELEFONOTextBox.Text)) = 0 Then
            MsgBox("Capture el Numero Telef�nico por favor ", MsgBoxStyle.Information)
            Exit Sub
        End If
        'If IsNumeric(Me.SectorComboBox.SelectedValue) = False Or Len(Me.SectorComboBox.Text) = 0 Then
        '    MsgBox("Seleccione el Sector por favor", MsgBoxStyle.Information)
        '    Exit Sub
        'End If

        'If SectorComboBox.Text.Length = 0 Then
        '    MessageBox.Show("Selecciona Sector.")
        '    Exit Sub
        'End If

        'If SectorComboBox.SelectedValue = 0 Then
        '    MessageBox.Show("Selecciona Sector.")
        '    Exit Sub
        'End If


        'Dim CON As New SqlConnection(MiConexion)
        'CON.Open()
        'Me.Validate()
        ''Me.CONCAMDOFACBindingSource.EndEdit()
        'Me.CONCAMDOFACTableAdapter.Connection = CON
        'Me.CONCAMDOFACTableAdapter.Update(gloClv_Session, GloContrato, CInt(Me.Clv_CalleComboBox.SelectedValue), Me.NUMEROTextBox.Text, Me.ENTRECALLESTextBox.Text, CInt(Me.Clv_ColoniaComboBox.SelectedValue), Me.TELEFONOTextBox.Text, 0, CInt(Me.Clv_CiudadComboBox.SelectedValue), CInt(Me.SectorComboBox.SelectedValue))
        ''Me.ConCAMDOTMPTableAdapter.Connection = CON
        ''Me.ConCAMDOTMPTableAdapter.Update(gloClv_Session, GloContrato, CInt(Me.Clv_CalleComboBox.SelectedValue), Me.NUMEROTextBox.Text, Me.ENTRECALLESTextBox.Text, CInt(Me.Clv_ColoniaComboBox.SelectedValue), Me.TELEFONOTextBox.Text, 0, CInt(Me.Clv_CiudadComboBox.SelectedValue))
        'CON.Close()
        GloBndExt = True
        'guardarSector(gloClv_Session, GloContrato, CInt(Me.Clv_CalleComboBox.SelectedValue), Me.NUMEROTextBox.Text, Me.ENTRECALLESTextBox.Text, CInt(Me.Clv_ColoniaComboBox.SelectedValue), Me.TELEFONOTextBox.Text, 0, CInt(Me.Clv_CiudadComboBox.SelectedValue), CInt(Me.SectorComboBox.SelectedValue))
        guardarSector(gloClv_Session, GloContrato, CInt(Me.Clv_CalleComboBox.SelectedValue), Me.NUMEROTextBox.Text, Me.ENTRECALLESTextBox.Text, CInt(Me.Clv_ColoniaComboBox.SelectedValue), Me.TELEFONOTextBox.Text, 0, CInt(Me.Clv_CiudadComboBox.SelectedValue), 0)
        Me.Close()
    End Sub
    Private Sub guardarSector(ByVal session As Long, ByVal contrato As Long, ByVal cvecalle As Integer, ByVal num As String, ByVal entrecalles As String, ByVal ccolonia As Integer, ByVal tel As String, ByVal ou As Decimal, ByVal cciu As Integer, ByVal sec As Integer)

        Dim CON As New SqlConnection(MiConexion)
        Dim CMD As New SqlCommand("ALTACAMDOFAC", CON)
        CMD.CommandType = CommandType.StoredProcedure

        Dim PRM1 As New SqlParameter("@ses", SqlDbType.BigInt)
        PRM1.Direction = ParameterDirection.Input
        PRM1.Value = session
        CMD.Parameters.Add(PRM1)

        Dim PRM2 As New SqlParameter("@con", SqlDbType.BigInt)
        PRM2.Direction = ParameterDirection.Input
        PRM2.Value = contrato
        CMD.Parameters.Add(PRM2)

        Dim PRM3 As New SqlParameter("@cal", SqlDbType.Int)
        PRM3.Direction = ParameterDirection.Input
        PRM3.Value = cvecalle
        CMD.Parameters.Add(PRM3)

        Dim PRM4 As New SqlParameter("@num", SqlDbType.VarChar, 100)
        PRM4.Direction = ParameterDirection.Input
        PRM4.Value = num
        CMD.Parameters.Add(PRM4)

        Dim PRM5 As New SqlParameter("@ent", SqlDbType.VarChar, 250)
        PRM5.Direction = ParameterDirection.Input
        PRM5.Value = entrecalles
        CMD.Parameters.Add(PRM5)

        Dim PRM6 As New SqlParameter("@col", SqlDbType.Int)
        PRM6.Direction = ParameterDirection.Input
        PRM6.Value = ccolonia
        CMD.Parameters.Add(PRM6)

        Dim PRM7 As New SqlParameter("@tel", SqlDbType.VarChar, 250)
        PRM7.Direction = ParameterDirection.Input
        PRM7.Value = tel
        CMD.Parameters.Add(PRM7)

        Dim PRM8 As New SqlParameter("@ou", SqlDbType.Decimal)
        PRM8.Direction = ParameterDirection.Input
        PRM8.Value = ou
        CMD.Parameters.Add(PRM8)

        Dim PRM9 As New SqlParameter("@ciu", SqlDbType.Int)
        PRM9.Direction = ParameterDirection.Input
        PRM9.Value = cciu
        CMD.Parameters.Add(PRM9)

        Dim PRM10 As New SqlParameter("@sec", SqlDbType.Int)
        PRM10.Direction = ParameterDirection.Input
        PRM10.Value = sec
        CMD.Parameters.Add(PRM10)

        Try
            CON.Open()
            CMD.ExecuteNonQuery()
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Information)
        Finally
            CON.Close()
            CON.Dispose()
        End Try
    End Sub

    Private Sub BindingNavigatorDeleteItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BindingNavigatorDeleteItem.Click
        Dim CON As New SqlConnection(MiConexion)

        'If IsNumeric(Me.CLAVETextBox.Text) = False Then
        '    MsgBox("No ahi Datos para eliminar ", MsgBoxStyle.Information)
        '    Exit Sub
        'End If
        If IsNumeric(Me.Clv_OrdenTextBox.Text) = False Then
            MsgBox("No hay Datos para eliminar ", MsgBoxStyle.Information)
            Exit Sub
        End If
        If IsNumeric(Me.CONTRATOTextBox.Text) = False Then
            MsgBox("No hay Datos para eliminar ", MsgBoxStyle.Information)
            Exit Sub
        End If
        CON.Open()
        Me.CONCAMDOFACTableAdapter.Delete(gloClv_Session, GloContrato)
        Me.CONCAMDOFACBindingSource.CancelEdit()
        'Me.ConCAMDOTMPTableAdapter.Connection = CON
        'Me.ConCAMDOTMPTableAdapter.Delete(gloClv_Session, GloContrato)
        CON.Close()

        GloBndExt = False
        Me.Close()
    End Sub

    Private Sub Clv_CiudadComboBox_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Clv_CiudadComboBox.SelectedIndexChanged
        If IsNumeric(Me.Clv_CiudadComboBox.SelectedValue) = True Then
            Me.Clv_CiudadComboBox.Enabled = True
        End If
    End Sub

    Private Sub Button5_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button5.Click
        GloBndExt = False
        Me.Close()
    End Sub

    Private Sub Clv_ColoniaComboBox_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Clv_ColoniaComboBox.SelectedIndexChanged
        Me.asignacolonia()
        Try
            Me.uspConsultaTap(0, CInt(Me.Clv_CalleComboBox.SelectedValue), CInt(Me.Clv_ColoniaComboBox.SelectedValue))
            If Clv_ColoniaComboBox.Text.Length > 0 Then
                MUESTRASector(0, Clv_ColoniaComboBox.SelectedValue)
            End If
        Catch ex As Exception

        End Try
    End Sub

    Private Sub Clv_CalleComboBox_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Clv_CalleComboBox.SelectedIndexChanged
        asiganacalle()
    End Sub
    Private Sub Busca()
        Try
            Dim CON As New SqlConnection(MiConexion)
            CON.Open()
            Me.CONCAMDOFACTableAdapter.Connection = CON
            Me.CONCAMDOFACTableAdapter.Fill(Me.NewsoftvDataSet2.CONCAMDOFAC, New System.Nullable(Of Long)(CType(gloClv_Session, Long)), New System.Nullable(Of Long)(CType(GloContrato, Long)))
            'Me.ConCAMDOTMPTableAdapter.Connection = CON
            'Me.ConCAMDOTMPTableAdapter.Fill(Me.DataSetEric3.ConCAMDOTMP, gloClv_Session, GloContrato)
            CON.Close()
        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try

    End Sub

    Private Sub asignacolonia()
        Try
            If IsNumeric(Me.Clv_ColoniaComboBox.SelectedValue) = True Then
                Dim CON As New SqlConnection(MiConexion)
                CON.Open()
                Me.MuestraCVECOLCIUTableAdapter.Connection = CON
                Me.MuestraCVECOLCIUTableAdapter.Fill(Me.NewsoftvDataSet2.MuestraCVECOLCIU, Me.Clv_ColoniaComboBox.SelectedValue)
                CON.Close()
                If opcionlocal = "N" Then Me.Clv_CiudadComboBox.Text = ""
                Me.Clv_CiudadComboBox.Enabled = True

            End If

        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)


        End Try
    End Sub
    Private Sub MUESTRASector(ByVal Contrato As Integer, ByVal Clv_Colonia As Integer)
        'BaseII.limpiaParametros()
        'BaseII.CreateMyParameter("@Contrato", SqlDbType.Int, Contrato)
        'BaseII.CreateMyParameter("@Clv_Colonia", SqlDbType.Int, Clv_Colonia)
        'SectorComboBox.DataSource = BaseII.ConsultaDT("MUESTRASectorCliente")
    End Sub
    Private Sub uspConsultaTap(ByVal prmContrato As Long, ByVal prmClvCalle As Integer, ByVal prmClvColonia As Integer)
        Dim CON As New SqlConnection(MiConexion)
        Dim STR As New StringBuilder

        STR.Append("EXEC uspConsultaTap ")
        STR.Append(CStr(prmContrato) & ", ")
        STR.Append(CStr(prmClvCalle) & ", ")
        STR.Append(CStr(prmClvColonia))

        Dim DT As New DataTable
        Dim DA As New SqlDataAdapter(STR.ToString, CON)

        Try
            CON.Open()
            DA.Fill(DT)
            Me.cmbTap.DataSource = DT
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Information)
        Finally
            CON.Close()
            CON.Dispose()
        End Try
    End Sub
    Private Sub asiganacalle()
        Try
            If IsNumeric(Me.Clv_CalleComboBox.SelectedValue) = True Then
                Dim CON As New SqlConnection(MiConexion)
                CON.Open()
                Me.DAMECOLONIA_CALLETableAdapter.Connection = CON
                Me.DAMECOLONIA_CALLETableAdapter.Fill(Me.NewsoftvDataSet2.DAMECOLONIA_CALLE, New System.Nullable(Of Integer)(CType(Me.Clv_CalleComboBox.SelectedValue, Integer)))
                CON.Close()
                Me.Clv_ColoniaComboBox.Enabled = True
                If opcionlocal = "N" Then Me.Clv_ColoniaComboBox.Text = ""
            End If

        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub SectorComboBox_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles SectorComboBox.SelectedIndexChanged

    End Sub

    Private Sub BindingSource1_CurrentChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BindingSource1.CurrentChanged

    End Sub
End Class