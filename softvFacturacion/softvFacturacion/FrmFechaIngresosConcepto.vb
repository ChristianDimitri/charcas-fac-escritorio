Imports System.Data.SqlClient

Public Class FrmFechaIngresosConcepto

    Private Sub FrmFechaIngresosConcepto_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'prueba
        colorea(Me)
        Me.DateTimePicker2.Value = Today
        Me.DateTimePicker1.Value = Today
        Me.DateTimePicker2.MaxDate = Today
    End Sub

    Private Sub DateTimePicker1_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles DateTimePicker1.ValueChanged
        Me.DateTimePicker2.MinDate = Me.DateTimePicker1.Value
    End Sub

    Private Sub DateTimePicker2_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles DateTimePicker2.ValueChanged
        Me.DateTimePicker1.MaxDate = Me.DateTimePicker2.Value
    End Sub

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        Me.Close()
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        eFechaInicial = Me.DateTimePicker1.Value
        eFechaFinal = Me.DateTimePicker2.Value
        If GloBnd_Des_Men = True Then
            FrmImprimirIngresosConcepto.Show()
        ElseIf LocbndPolizaCiudad = True Then
            GeneraPolizaNew()
            Locbndactualizapoiza = True
        Else
            FrmImprimirIngresosConcepto.Show()
        End If
        Me.Close()
    End Sub

    Private Sub GeneraPoliza()
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        Dim comando As SqlClient.SqlCommand
        comando = New SqlClient.SqlCommand
        With comando
            .Connection = CON
            .CommandText = "GeneraPoliza "
            .CommandType = CommandType.StoredProcedure
            .CommandTimeout = 0
            '  varchar(10)
            Dim prm As New SqlParameter("@Fecha_Ini", SqlDbType.DateTime)
            prm.Direction = ParameterDirection.Input
            prm.Value = eFechaInicial
            .Parameters.Add(prm)

            Dim prm1 As New SqlParameter("@Fecha_Fin", SqlDbType.DateTime)
            prm1.Direction = ParameterDirection.Input
            prm1.Value = eFechaFinal
            .Parameters.Add(prm1)

            Dim prm2 As New SqlParameter("@Tipo", SqlDbType.VarChar, 1)
            prm2.Direction = ParameterDirection.Input
            prm2.Value = "C"
            .Parameters.Add(prm2)

            Dim prm3 As New SqlParameter("@sucursal", SqlDbType.Int)
            prm3.Direction = ParameterDirection.Input
            prm3.Value = 0
            .Parameters.Add(prm3)

            Dim prm4 As New SqlParameter("@Caja", SqlDbType.Int)
            prm4.Direction = ParameterDirection.Input
            prm4.Value = 0
            .Parameters.Add(prm4)

            Dim prm5 As New SqlParameter("@Cajera", SqlDbType.VarChar)
            prm5.Direction = ParameterDirection.Input
            prm5.Value = GloUsuario
            .Parameters.Add(prm5)

            Dim prm6 As New SqlParameter("@Op", SqlDbType.VarChar)
            prm6.Direction = ParameterDirection.Input
            prm6.Value = 0
            .Parameters.Add(prm6)

            Dim prm7 As New SqlParameter("@Clv_Session", SqlDbType.BigInt)
            prm7.Direction = ParameterDirection.Input
            prm7.Value = gloClv_Session
            .Parameters.Add(prm7)

            Dim prm8 As New SqlParameter("@Clv_Usuario", SqlDbType.VarChar, 11)
            prm8.Direction = ParameterDirection.Input
            prm8.Value = GloUsuario
            .Parameters.Add(prm8)

            Dim prm9 As New SqlParameter("Clv_llave_Poliza", SqlDbType.BigInt)
            prm9.Direction = ParameterDirection.InputOutput
            prm9.Value = 0
            .Parameters.Add(prm9)

            Dim i As Integer = comando.ExecuteNonQuery()
            LocGloClv_poliza = prm9.Value
            LocopPoliza = "N"
        End With
        bitsist(GloUsuario, 0, GloSistema, Me.Name, "", "Se Genero Poliza", "Con Clave de poliza de: " + CStr(LocGloClv_poliza), LocClv_Ciudad)
        Dim ComandoLidia As New SqlClient.SqlCommand
        With ComandoLidia
            .CommandText = "Dame_Clave_poliza"
            .CommandTimeout = 0
            .CommandType = CommandType.StoredProcedure
            .Connection = CON
            Dim Prm1 As New SqlParameter("@Clv_Llave_Poliza", SqlDbType.BigInt)
            Prm1.Direction = ParameterDirection.Input
            Prm1.Value = LocGloClv_poliza
            .Parameters.Add(Prm1)

            Dim Prm2 As New SqlParameter("@opc", SqlDbType.VarChar, 5)
            Prm2.Direction = ParameterDirection.Input
            Prm2.Value = LocopPoliza
            .Parameters.Add(Prm2)

            Dim Prm3 As New SqlParameter("@Clave", SqlDbType.BigInt)
            Prm3.Direction = ParameterDirection.Output
            Prm3.Value = 0
            .Parameters.Add(Prm3)

            Dim i As Integer = ComandoLidia.ExecuteNonQuery()
            GloPoliza2 = Prm3.Value

        End With
        CON.Close()
        FrmPoliza.Show()
    End Sub

    Private Sub GeneraPolizaNew()
        Dim NumError As Integer = 0
        Dim MsjError As String = Nothing


        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        Dim comando As SqlClient.SqlCommand
        comando = New SqlClient.SqlCommand
        With comando
            .Connection = CON
            .CommandText = "GeneraPoliza_New "
            .CommandType = CommandType.StoredProcedure
            .CommandTimeout = 0
            '  varchar(10)
            Dim prm As New SqlParameter("@Fecha_Ini", SqlDbType.DateTime)
            prm.Direction = ParameterDirection.Input
            prm.Value = eFechaInicial
            .Parameters.Add(prm)

            Dim prm1 As New SqlParameter("@Fecha_Fin", SqlDbType.DateTime)
            prm1.Direction = ParameterDirection.Input
            prm1.Value = eFechaFinal
            .Parameters.Add(prm1)

            Dim prm2 As New SqlParameter("@Tipo", SqlDbType.VarChar, 1)
            prm2.Direction = ParameterDirection.Input
            prm2.Value = "C"
            .Parameters.Add(prm2)

            Dim prm3 As New SqlParameter("@sucursal", SqlDbType.Int)
            prm3.Direction = ParameterDirection.Input
            prm3.Value = ciudades
            .Parameters.Add(prm3)

            Dim prm4 As New SqlParameter("@Caja", SqlDbType.Int)
            prm4.Direction = ParameterDirection.Input
            prm4.Value = 0
            .Parameters.Add(prm4)

            Dim prm5 As New SqlParameter("@Cajera", SqlDbType.VarChar)
            prm5.Direction = ParameterDirection.Input
            prm5.Value = GloUsuario
            .Parameters.Add(prm5)

            Dim prm6 As New SqlParameter("@Op", SqlDbType.Int)
            prm6.Direction = ParameterDirection.Input
            prm6.Value = 0
            .Parameters.Add(prm6)

            Dim prm7 As New SqlParameter("@Clv_Session", SqlDbType.BigInt)
            prm7.Direction = ParameterDirection.Input
            prm7.Value = gloClv_Session
            .Parameters.Add(prm7)

            Dim prm8 As New SqlParameter("@Clv_Usuario", SqlDbType.VarChar, 10)
            prm8.Direction = ParameterDirection.Input
            prm8.Value = GloUsuario
            .Parameters.Add(prm8)

            Dim prm9 As New SqlParameter("@Clv_llave_PolizaNew", SqlDbType.BigInt)
            prm9.Direction = ParameterDirection.Output
            prm9.Value = 0
            .Parameters.Add(prm9)

            Dim prm10 As New SqlParameter("@NumError", SqlDbType.Int)
            prm10.Direction = ParameterDirection.Output
            prm10.Value = 0
            .Parameters.Add(prm10)

            Dim prm11 As New SqlParameter("@MsjError", SqlDbType.VarChar, 250)
            prm11.Direction = ParameterDirection.Output
            prm11.Value = 0
            .Parameters.Add(prm11)

            Dim i As Integer = comando.ExecuteNonQuery()
            LocGloClv_poliza = prm9.Value
            'NumError = prm10.Value
            'MsjError = prm11.Value
            LocopPoliza = "N"
        End With
        Dim cmd As New SqlCommand()
        bitsist(GloUsuario, 0, GloSistema, Me.Name, "", "Se Genero Poliza", "Con Clave de poliza de: " + CStr(LocGloClv_poliza), LocClv_Ciudad)
        If NumError = 1 Then
            MsgBox(MsjError, MsgBoxStyle.Exclamation)
            'ElseIf NumError = 2 Then
            '    MsgBox(MsjError & ".  Presione aceptar para seleccionar la ruta donde se guardar� el archivo con los errores detectados  ", MsgBoxStyle.Exclamation)
            '    Dim result As DialogResult = FolderBrowserDialog1.ShowDialog()
            '    Dim Nom_Archivo As String = Nothing
            '    Dim Rutatxt As String = Nothing
            '    If (result = DialogResult.OK) Then
            '        Rutatxt = Me.FolderBrowserDialog1.SelectedPath.ToString
            '        Nom_Archivo = Rutatxt + "\" + "erropoliza" & gloClv_Session & ".txt"
            '        Dim fileExists As Boolean
            '        fileExists = My.Computer.FileSystem.FileExists(Nom_Archivo)
            '        If fileExists = True Then
            '            File.Delete(Nom_Archivo)
            '        End If
            '        Using sw As StreamWriter = File.CreateText(Nom_Archivo)

            '            cmd = New SqlClient.SqlCommand()

            '            With cmd
            '                .CommandText = "Select Clv_Session,Clv_TipSer,Clv_Servicio, Conceto,Importe from Tabla_Conceptos_NoExistentes Where Clv_Session = " & gloClv_Session
            '                .Connection = CON
            '                .CommandTimeout = 0
            '                .CommandType = CommandType.Text
            '                Dim lector As SqlDataReader = .ExecuteReader()

            '                While lector.Read()
            '                    sw.WriteLine("Articulo sin numero de Cuenta : " & lector(3).ToString())
            '                End While
            '            End With
            '            CON.Close()

            '        End Using
            '    End If
            '    MsgBox("El  archivo fue guardado exitosamente en " & Nom_Archivo, MsgBoxStyle.Information)
        ElseIf NumError = 0 Then
            Dim ComandoLidia As New SqlClient.SqlCommand
            With ComandoLidia
                .CommandText = "Dame_Clave_poliza"
                .CommandTimeout = 0
                .CommandType = CommandType.StoredProcedure
                .Connection = CON
                Dim Prm1 As New SqlParameter("@Clv_Llave_Poliza", SqlDbType.BigInt)
                Prm1.Direction = ParameterDirection.Input
                Prm1.Value = LocGloClv_poliza
                .Parameters.Add(Prm1)

                Dim Prm2 As New SqlParameter("@opc", SqlDbType.VarChar, 5)
                Prm2.Direction = ParameterDirection.Input
                Prm2.Value = LocopPoliza
                .Parameters.Add(Prm2)

                Dim Prm3 As New SqlParameter("@Clave", SqlDbType.BigInt)
                Prm3.Direction = ParameterDirection.Output
                Prm3.Value = 0
                .Parameters.Add(Prm3)

                Dim i As Integer = ComandoLidia.ExecuteNonQuery()
                GloPoliza2 = Prm3.Value

            End With
            CON.Close()
            FrmPoliza.Show()
        End If
        If CON.State = ConnectionState.Open Then CON.Close()
    End Sub

End Class