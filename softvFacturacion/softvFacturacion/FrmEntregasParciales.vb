Imports System.Data.SqlClient

Public Class FrmEntregasParciales

    Private eClave As String = Nothing
    Private eFecha As String = Nothing
    Private eHora As String = Nothing
    Private eRecibio As String = Nothing
    Private eCajera As String = Nothing
    Private eCepsa As String = Nothing
    Private eReferencia As String = Nothing
    Private eB1000 As String = Nothing
    Private eB500 As String = Nothing
    Private eB200 As String = Nothing
    Private eB100 As String = Nothing
    Private eB50 As String = Nothing
    Private eB20 As String = Nothing
    Private eM100 As String = Nothing
    Private eM50 As String = Nothing
    Private eM20 As String = Nothing
    Private eM10 As String = Nothing
    Private eM5 As String = Nothing
    Private eM2 As String = Nothing
    Private eM1 As String = Nothing
    Private eM050 As String = Nothing
    Private eM020 As String = Nothing
    Private eM010 As String = Nothing
    Private eM005 As String = Nothing
    Private eImporte As String = Nothing

    Private Sub DameDatosBitacora()
        Try
            If op = "M" Then
                eClave = Me.ConsecutivoTextBox.Text
                eFecha = Me.FechaDateTimePicker.Text
                eHora = Me.HoraDateTimePicker.Text
                eRecibio = Me.RecibioTextBox.Text
                eCajera = Me.ComboBox1.Text
                eCepsa = Me.NumeroCepsaTextBox.Text
                eReferencia = Me.ReferenciaTextBox.Text
                eB1000 = Me.B1000TextBox.Text
                eB500 = Me.B500TextBox.Text
                eB200 = Me.B200TextBox.Text
                eB100 = Me.B100TextBox.Text
                eB50 = Me.B50TextBox.Text
                eB20 = Me.B20TextBox.Text
                eM100 = Me.M100TextBox.Text
                eM50 = Me.M50TextBox.Text
                eM20 = Me.M20TextBox.Text
                eM10 = Me.M10TextBox.Text
                eM5 = Me.M5TextBox.Text
                eM2 = Me.M2TextBox.Text
                eM1 = Me.M1TextBox.Text
                eM050 = Me.M050TextBox.Text
                eM020 = Me.M020TextBox.Text
                eM010 = Me.M010TextBox.Text
                eM005 = Me.M005TextBox.Text
                eImporte = Me.ImporteTextBox.Text
            End If
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub
    Private Sub GuardaDatosBitacora(ByVal Opcion As Integer)
        Try
            If Opcion = 0 Then
                If op = "M" Then
                    bitsist(GloUsuario, 0, GloSistema, Me.Text, "Consecutivo: " + Me.ConsecutivoTextBox.Text, "Fecha :" + eFecha, "Fecha :" + Me.FechaDateTimePicker.Text, LocClv_Ciudad)
                    bitsist(GloUsuario, 0, GloSistema, Me.Text, "Consecutivo: " + Me.ConsecutivoTextBox.Text, "Hora: " + eHora, "Hora: " + Me.HoraDateTimePicker.Text, LocClv_Ciudad)
                    bitsist(GloUsuario, 0, GloSistema, Me.Text, "Consecutivo: " + Me.ConsecutivoTextBox.Text, "Recibi�: " + eRecibio, "Recibi�: " + Me.RecibioTextBox.Text, LocClv_Ciudad)
                    bitsist(GloUsuario, 0, GloSistema, Me.Text, "Consecutivo: " + Me.ConsecutivoTextBox.Text, "Cajera: " + eCajera, "Cajera: " + Me.ComboBox1.Text, LocClv_Ciudad)
                    bitsist(GloUsuario, 0, GloSistema, Me.Text, "Consecutivo: " + Me.ConsecutivoTextBox.Text, "N�mero de Cepsa: " + eCepsa, "N�mero de Cepsa: " + Me.NumeroCepsaTextBox.Text, LocClv_Ciudad)
                    bitsist(GloUsuario, 0, GloSistema, Me.Text, "Consecutivo: " + Me.ConsecutivoTextBox.Text, "Referencia: " + eReferencia, "Referencia: " + Me.ReferenciaTextBox.Text, LocClv_Ciudad)
                    bitsist(GloUsuario, 0, GloSistema, Me.Text, "Consecutivo: " + Me.ConsecutivoTextBox.Text, "B1000: " + eB1000, "B1000: " + Me.B1000TextBox.Text, LocClv_Ciudad)
                    bitsist(GloUsuario, 0, GloSistema, Me.Text, "Consecutivo: " + Me.ConsecutivoTextBox.Text, "B500: " + eB500, "B500: " + Me.B500TextBox.Text, LocClv_Ciudad)
                    bitsist(GloUsuario, 0, GloSistema, Me.Text, "Consecutivo: " + Me.ConsecutivoTextBox.Text, "B200: " + eB200, "B200: " + Me.B200TextBox.Text, LocClv_Ciudad)
                    bitsist(GloUsuario, 0, GloSistema, Me.Text, "Consecutivo: " + Me.ConsecutivoTextBox.Text, "B100: " + eB100, "B100: " + Me.B100TextBox.Text, LocClv_Ciudad)
                    bitsist(GloUsuario, 0, GloSistema, Me.Text, "Consecutivo: " + Me.ConsecutivoTextBox.Text, "B50: " + eB50, "B50: " + Me.B50TextBox.Text, LocClv_Ciudad)
                    bitsist(GloUsuario, 0, GloSistema, Me.Text, "Consecutivo: " + Me.ConsecutivoTextBox.Text, "B20: " + eB20, "B20: " + Me.B20TextBox.Text, LocClv_Ciudad)
                    bitsist(GloUsuario, 0, GloSistema, Me.Text, "Consecutivo: " + Me.ConsecutivoTextBox.Text, "M100: " + eM100, "M100: " + Me.M100TextBox.Text, LocClv_Ciudad)
                    bitsist(GloUsuario, 0, GloSistema, Me.Text, "Consecutivo: " + Me.ConsecutivoTextBox.Text, "M50: " + eM50, "M50: " + Me.M50TextBox.Text, LocClv_Ciudad)
                    bitsist(GloUsuario, 0, GloSistema, Me.Text, "Consecutivo: " + Me.ConsecutivoTextBox.Text, "M20: " + eM20, "M20: " + Me.M20TextBox.Text, LocClv_Ciudad)
                    bitsist(GloUsuario, 0, GloSistema, Me.Text, "Consecutivo: " + Me.ConsecutivoTextBox.Text, "M10: " + eM10, "M10: " + Me.M10TextBox.Text, LocClv_Ciudad)
                    bitsist(GloUsuario, 0, GloSistema, Me.Text, "Consecutivo: " + Me.ConsecutivoTextBox.Text, "M5: " + eM5, "M5: " + Me.M5TextBox.Text, LocClv_Ciudad)
                    bitsist(GloUsuario, 0, GloSistema, Me.Text, "Consecutivo: " + Me.ConsecutivoTextBox.Text, "M1: " + eM1, "M1: " + Me.M1TextBox.Text, LocClv_Ciudad)
                    bitsist(GloUsuario, 0, GloSistema, Me.Text, "Consecutivo: " + Me.ConsecutivoTextBox.Text, "M050: " + eM050, "M050: " + Me.M050TextBox.Text, LocClv_Ciudad)
                    bitsist(GloUsuario, 0, GloSistema, Me.Text, "Consecutivo: " + Me.ConsecutivoTextBox.Text, "M020: " + eM020, "M020: " + Me.M020TextBox.Text, LocClv_Ciudad)
                    bitsist(GloUsuario, 0, GloSistema, Me.Text, "Consecutivo: " + Me.ConsecutivoTextBox.Text, "M010: " + eM010, "M010: " + Me.M010TextBox.Text, LocClv_Ciudad)
                    bitsist(GloUsuario, 0, GloSistema, Me.Text, "Consecutivo: " + Me.ConsecutivoTextBox.Text, "M005: " + eM005, "M005: " + Me.M005TextBox.Text, LocClv_Ciudad)
                    bitsist(GloUsuario, 0, GloSistema, Me.Text, "Consecutivo: " + Me.ConsecutivoTextBox.Text, "M005: " + eM005, "M005: " + Me.M005TextBox.Text, LocClv_Ciudad)
                    bitsist(GloUsuario, 0, GloSistema, Me.Text, "Consecutivo: " + Me.ConsecutivoTextBox.Text, "Importe: " + eImporte, "Importe: " + Me.ImporteTextBox.Text, LocClv_Ciudad)
                ElseIf op = "N" Then
                    bitsist(GloUsuario, 0, GloSistema, Me.Text, "Nueva Entrega Parcial", "", "Consecutivo: " + Me.ConsecutivoTextBox.Text, LocClv_Ciudad)
                End If
            ElseIf Opcion = 1 Then
                bitsist(GloUsuario, 0, GloSistema, Me.Text, "Se Elimin� Entrega Parcial", "Consecutivo: " + Me.ConsecutivoTextBox.Text, "", LocClv_Ciudad)
            End If
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub



    Private Sub CONPARCIALESBindingNavigatorSaveItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        Me.Validate()
        Me.CONPARCIALESBindingSource.EndEdit()
        Me.CONPARCIALESTableAdapter.Connection = CON
        Me.CONPARCIALESTableAdapter.Update(Me.NewsoftvDataSet1.CONPARCIALES)
        CON.Close()

    End Sub

    Private Sub BuscaParcial(ByVal GloConsecutivo As Long)
        Try
            Dim CON As New SqlConnection(MiConexion)
            CON.Open()
            Me.CONPARCIALESTableAdapter.Connection = CON
            Me.CONPARCIALESTableAdapter.Fill(Me.NewsoftvDataSet1.CONPARCIALES, New System.Nullable(Of Long)(CType(GloConsecutivo, Long)))
            CON.Close()
        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try

    End Sub

    Private Sub CONPARCIALESBindingNavigator_RefreshItems(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CONPARCIALESBindingNavigator.RefreshItems

    End Sub

    Private Sub Panel1_Paint(ByVal sender As System.Object, ByVal e As System.Windows.Forms.PaintEventArgs) Handles Panel1.Paint

    End Sub

    Private Sub FrmEntregasParciales_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        colorea(Me)
        'Me.RecibioTextBox.Enabled = False
        If op = "N" Then
            Me.CONPARCIALESBindingSource.AddNew()
            Me.Label3.Text = "Nueva Entrega Parcial"
            Dim CON As New SqlConnection(MiConexion)
            CON.Open()
            Me.MUESTRAUSUARIOS2TableAdapter.Connection = CON
            Me.MUESTRAUSUARIOS2TableAdapter.Fill(Me.NewsoftvDataSet1.MUESTRAUSUARIOS2, 21)
            CON.Close()
            Me.RecibioTextBox.Text = GloClv_Txt
            'Me.ComboBox1.Text = ""
            Me.ComboBox1.Text = GloUsuario
        ElseIf op = "C" Then
            Me.BuscaParcial(Consecutivo)
            Me.Panel1.Enabled = False
            Me.Label3.Text = "Consulta Entregas Parciales"
            Me.RecibioTextBox.Text = GloClv_Txt
            Me.ComboBox1.Text = GloCajera
            CONEntregasParcialesDolares(Consecutivo)
        ElseIf op = "M" Then
            Me.BuscaParcial(Consecutivo)
            Me.Label3.Text = "Modificaciones De Una Entrega Parcial"
            Dim CON2 As New SqlConnection(MiConexion)
            CON2.Open()
            Me.MUESTRAUSUARIOS2TableAdapter.Connection = CON2
            Me.MUESTRAUSUARIOS2TableAdapter.Fill(Me.NewsoftvDataSet1.MUESTRAUSUARIOS2, 21)
            CON2.Close()
            Me.ComboBox1.Text = GloCajera
            Me.RecibioTextBox.Text = GloClv_Txt
            DameDatosBitacora()
            CONEntregasParcialesDolares(Consecutivo)
        End If

    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        Me.Close()
    End Sub

    Private Sub ImporteTextBox_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ImporteTextBox.TextChanged

    End Sub
    Private Sub Suma_Detalle()
        Dim b1000 As Integer = 0
        Dim b500 As Integer = 0
        Dim b200 As Integer = 0
        Dim b100 As Integer = 0
        Dim b20 As Integer = 0
        Dim b50 As Integer = 0
        Dim M100 As Integer = 0
        Dim M50 As Integer = 0
        Dim M20 As Integer = 0
        Dim M10 As Integer = 0
        Dim M5 As Integer = 0
        Dim M2 As Integer = 0
        Dim M1 As Integer = 0
        Dim M050 As Double = 0
        Dim M020 As Double = 0
        Dim M010 As Double = 0
        Dim M005 As Double = 0
        Dim Tarjeta As Double = 0
        Dim Cheque As Double = 0
        Dim ImporteDolares As Double = 0
        Dim TipoCambio As Double = 0
        Dim ConversionPesos As Double = 0

        Try

            If IsNumeric(Me.B1000TextBox.Text) = True Then
                b1000 = Me.B1000TextBox.Text * 1000
            End If
            If IsNumeric(Me.B500TextBox.Text) = True Then
                b500 = Me.B500TextBox.Text * 500
            End If
            If IsNumeric(Me.B200TextBox.Text) = True Then
                b200 = Me.B200TextBox.Text * 200
            End If
            If IsNumeric(Me.B100TextBox.Text) = True Then
                b100 = Me.B100TextBox.Text * 100
            End If
            If IsNumeric(Me.B20TextBox.Text) = True Then
                b20 = Me.B20TextBox.Text * 20
            End If
            If IsNumeric(Me.B50TextBox.Text) = True Then
                b50 = Me.B50TextBox.Text * 50
            End If
            If IsNumeric(Me.M100TextBox.Text) = True Then
                M100 = Me.M100TextBox.Text * 100
            End If
            If IsNumeric(Me.M50TextBox.Text) = True Then
                M50 = Me.M50TextBox.Text * 50
            End If
            If IsNumeric(Me.M20TextBox.Text) = True Then
                M20 = Me.M20TextBox.Text * 20
            End If
            If IsNumeric(Me.M10TextBox.Text) = True Then
                M10 = Me.M10TextBox.Text * 10
            End If
            If IsNumeric(Me.M5TextBox.Text) = True Then
                M5 = Me.M5TextBox.Text * 5
            End If
            If IsNumeric(Me.M2TextBox.Text) = True Then
                M2 = Me.M2TextBox.Text * 2
            End If
            If IsNumeric(Me.M1TextBox.Text) = True Then
                M1 = Me.M1TextBox.Text * 1
            End If
            If IsNumeric(Me.M050TextBox.Text) = True Then
                M050 = Me.M050TextBox.Text * 0.5
            End If
            If IsNumeric(Me.M020TextBox.Text) = True Then
                M020 = Me.M020TextBox.Text * 0.2
            End If
            If IsNumeric(Me.M010TextBox.Text) = True Then
                M010 = Me.M010TextBox.Text * 0.1
            End If
            If IsNumeric(Me.M005TextBox.Text) = True Then
                M005 = Me.M005TextBox.Text * 0.05
            End If
            If IsNumeric(Me.ChequesTextBox.Text) = True Then
                Cheque = Me.ChequesTextBox.Text
            End If
            If IsNumeric(Me.TarjetaTextBox.Text) = True Then
                Tarjeta = Me.TarjetaTextBox.Text
            End If

            If IsNumeric(tbImporteDolares.Text) = True Then
                ImporteDolares = tbImporteDolares.Text
            End If
            If IsNumeric(tbTipoCambio.Text) = True Then
                TipoCambio = tbTipoCambio.Text
            End If

            ConversionPesos = ImporteDolares * TipoCambio
            tbConversionPesos.Text = ConversionPesos

            Suma = b1000 + b500 + b200 + b100 + b50 + b20 + M100 + M50 + M20 + M10 + M5 + M2 + M1 + M050 + M020 + M010 + M005 + Tarjeta + Cheque
            Me.ImporteTextBox.Text = Suma

            tbGranTotal.Text = Suma + ConversionPesos

        Catch
            MsgBox("Demasiado Dinero, Rectifica", , "Advertencia")
        End Try


    End Sub

    Private Sub B1000TextBox_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles B1000TextBox.KeyPress
        e.KeyChar = Chr((ValidaKey(Me.B1000TextBox, Asc(LCase(e.KeyChar)), "N")))
    End Sub

    Private Sub B1000TextBox_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles B1000TextBox.LostFocus

    End Sub

    Private Sub B1000TextBox_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles B1000TextBox.TextChanged
        Suma_Detalle()
    End Sub

    Private Sub B500TextBox_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles B500TextBox.KeyPress
        e.KeyChar = Chr((ValidaKey(Me.B500TextBox, Asc(LCase(e.KeyChar)), "N")))
    End Sub

    Private Sub B500TextBox_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles B500TextBox.TextChanged
        Suma_Detalle()
    End Sub

    Private Sub B200TextBox_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles B200TextBox.KeyPress
        e.KeyChar = Chr((ValidaKey(Me.B200TextBox, Asc(LCase(e.KeyChar)), "N")))
    End Sub

    Private Sub B200TextBox_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles B200TextBox.TextChanged
        Suma_Detalle()
    End Sub

    Private Sub B100TextBox_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles B100TextBox.KeyPress
        e.KeyChar = Chr((ValidaKey(Me.B100TextBox, Asc(LCase(e.KeyChar)), "N")))
    End Sub

    Private Sub B100TextBox_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles B100TextBox.TextChanged
        Suma_Detalle()
    End Sub

    Private Sub B50TextBox_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles B50TextBox.KeyPress
        e.KeyChar = Chr((ValidaKey(Me.B50TextBox, Asc(LCase(e.KeyChar)), "N")))
    End Sub

    Private Sub B50TextBox_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles B50TextBox.TextChanged
        Suma_Detalle()
    End Sub

    Private Sub B20TextBox_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles B20TextBox.KeyPress
        e.KeyChar = Chr((ValidaKey(Me.B20TextBox, Asc(LCase(e.KeyChar)), "N")))
    End Sub

    Private Sub B20TextBox_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles B20TextBox.TextChanged
        Suma_Detalle()
    End Sub

    Private Sub M100TextBox_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles M100TextBox.KeyPress
        e.KeyChar = Chr((ValidaKey(Me.M100TextBox, Asc(LCase(e.KeyChar)), "N")))
    End Sub

    Private Sub M100TextBox_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles M100TextBox.TextChanged
        Suma_Detalle()
    End Sub

    Private Sub M50TextBox_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles M50TextBox.KeyPress
        e.KeyChar = Chr((ValidaKey(Me.B50TextBox, Asc(LCase(e.KeyChar)), "N")))
    End Sub

    Private Sub M50TextBox_RegionChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles M50TextBox.RegionChanged

    End Sub

    Private Sub M50TextBox_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles M50TextBox.TextChanged
        Suma_Detalle()
    End Sub

    Private Sub M20TextBox_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles M20TextBox.KeyPress
        e.KeyChar = Chr((ValidaKey(Me.B20TextBox, Asc(LCase(e.KeyChar)), "N")))
    End Sub

    Private Sub M20TextBox_RegionChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles M20TextBox.RegionChanged

    End Sub

    Private Sub M20TextBox_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles M20TextBox.TextChanged
        Suma_Detalle()
    End Sub

    Private Sub M10TextBox_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles M10TextBox.KeyPress
        e.KeyChar = Chr((ValidaKey(Me.M10TextBox, Asc(LCase(e.KeyChar)), "N")))
    End Sub

    Private Sub M10TextBox_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles M10TextBox.TextChanged
        Suma_Detalle()
    End Sub

    Private Sub M5TextBox_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles M5TextBox.KeyPress
        e.KeyChar = Chr((ValidaKey(Me.M5TextBox, Asc(LCase(e.KeyChar)), "N")))
    End Sub

    Private Sub M5TextBox_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles M5TextBox.TextChanged
        Suma_Detalle()
    End Sub

    Private Sub M2TextBox_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles M2TextBox.KeyPress
        e.KeyChar = Chr((ValidaKey(Me.M2TextBox, Asc(LCase(e.KeyChar)), "N")))
    End Sub

    Private Sub M2TextBox_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles M2TextBox.TextChanged
        Suma_Detalle()
    End Sub

    Private Sub M1TextBox_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles M1TextBox.KeyPress
        e.KeyChar = Chr((ValidaKey(Me.M1TextBox, Asc(LCase(e.KeyChar)), "N")))
    End Sub

    Private Sub M1TextBox_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles M1TextBox.TextChanged
        Suma_Detalle()
    End Sub

    Private Sub M050TextBox_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles M050TextBox.KeyPress
        e.KeyChar = Chr((ValidaKey(Me.M050TextBox, Asc(LCase(e.KeyChar)), "N")))
    End Sub

    Private Sub M050TextBox_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles M050TextBox.TextChanged
        Suma_Detalle()
    End Sub

    Private Sub M020TextBox_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles M020TextBox.KeyPress
        e.KeyChar = Chr((ValidaKey(Me.M20TextBox, Asc(LCase(e.KeyChar)), "N")))
    End Sub

    Private Sub M020TextBox_KeyUp(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles M020TextBox.KeyUp

    End Sub

    Private Sub M020TextBox_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles M020TextBox.TextChanged
        Suma_Detalle()
    End Sub

    Private Sub M010TextBox_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles M010TextBox.KeyPress
        e.KeyChar = Chr((ValidaKey(Me.M010TextBox, Asc(LCase(e.KeyChar)), "N")))
    End Sub

    Private Sub M010TextBox_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles M010TextBox.TextChanged
        Suma_Detalle()
    End Sub

    Private Sub M005TextBox_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles M005TextBox.KeyPress
        e.KeyChar = Chr((ValidaKey(Me.M005TextBox, Asc(LCase(e.KeyChar)), "N")))
    End Sub

    Private Sub M005TextBox_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles M005TextBox.TextChanged
        Suma_Detalle()
    End Sub

    Private Sub CONPARCIALESBindingNavigatorSaveItem_Click_1(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CONPARCIALESBindingNavigatorSaveItem.Click

        If IsNumeric(Me.B1000TextBox.Text) = False Then
            Me.B1000TextBox.Text = 0
        End If
        If IsNumeric(Me.B500TextBox.Text) = False Then
            Me.B500TextBox.Text = 0
        End If
        If IsNumeric(Me.B200TextBox.Text) = False Then
            Me.B200TextBox.Text = 0
        End If
        If IsNumeric(Me.B100TextBox.Text) = False Then
            Me.B100TextBox.Text = 0
        End If
        If IsNumeric(Me.B20TextBox.Text) = False Then
            Me.B20TextBox.Text = 0
        End If
        If IsNumeric(Me.B50TextBox.Text) = False Then
            Me.B50TextBox.Text = 0
        End If
        If IsNumeric(Me.M100TextBox.Text) = False Then
            Me.M100TextBox.Text = 0
        End If
        If IsNumeric(Me.M50TextBox.Text) = False Then
            Me.M50TextBox.Text = 0
        End If
        If IsNumeric(Me.M20TextBox.Text) = False Then
            Me.M20TextBox.Text = 0
        End If
        If IsNumeric(Me.M10TextBox.Text) = False Then
            Me.M10TextBox.Text = 0
        End If
        If IsNumeric(Me.M5TextBox.Text) = False Then
            Me.M5TextBox.Text = 0
        End If
        If IsNumeric(Me.M2TextBox.Text) = False Then
            Me.M2TextBox.Text = 0
        End If
        If IsNumeric(Me.M1TextBox.Text) = False Then
            Me.M1TextBox.Text = 0
        End If
        If IsNumeric(Me.M050TextBox.Text) = False Then
            Me.M050TextBox.Text = 0
        End If
        If IsNumeric(Me.M020TextBox.Text) = False Then
            Me.M020TextBox.Text = 0
        End If
        If IsNumeric(Me.M010TextBox.Text) = False Then
            Me.M010TextBox.Text = 0
        End If
        If IsNumeric(Me.M005TextBox.Text) = False Then
            Me.M005TextBox.Text = 0
        End If
        If IsNumeric(Me.ChequesTextBox.Text) = False Then
            Me.ChequesTextBox.Text = 0
        End If
        If IsNumeric(Me.TarjetaTextBox.Text) = False Then
            Me.TarjetaTextBox.Text = 0
        End If

        If IsNumeric(tbImporteDolares.Text) = False Then
            tbImporteDolares.Text = 0
        End If

        If IsNumeric(tbTipoCambio.Text) = False Then
            tbTipoCambio.Text = 0
        End If

        If IsNumeric(tbConversionPesos.Text) = False Then
            tbConversionPesos.Text = 0
        End If

        If CDec(tbImporteDolares.Text) > 0 And CDec(tbTipoCambio.Text) = 0 Then
            MessageBox.Show("Captura el Tipo de Cambio.")
            Exit Sub
        End If

        If CDec(tbImporteDolares.Text) = 0 And CDec(tbTipoCambio.Text) > 0 Then
            MessageBox.Show("Captura el Importe en D�lares.")
            Exit Sub
        End If

        If Me.ComboBox1.Text = "" Then
            MsgBox("Selecciona Primero un(a) Cajero(a)", MsgBoxStyle.Information, "Alerta")
        Else
            Dim CON3 As New SqlConnection(MiConexion)
            CON3.Open()
            Me.Validate()
            Me.CONPARCIALESBindingSource.EndEdit()
            Me.CONPARCIALESTableAdapter.Connection = CON3
            Me.CONPARCIALESTableAdapter.Update(Me.NewsoftvDataSet1.CONPARCIALES)
            CON3.Close()
            GuardaDatosBitacora(0)
            GloBnd = True

            MsgBox("Los Cambios Fueron Guardados", MsgBoxStyle.Information, "Guardar")

            GloConsecutivo = CInt(Me.ConsecutivoTextBox.Text)
            NUEEntregasParcialesDolares(GloConsecutivo, tbImporteDolares.Text, tbTipoCambio.Text)
            GloReporte = 2

            My.Forms.FrmImprimirRepGral.Show()
            Me.Close()
        End If
    End Sub

    Private Sub ConsecutivoTextBox_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ConsecutivoTextBox.TextChanged

    End Sub

    Private Sub BindingNavigatorDeleteItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BindingNavigatorDeleteItem.Click
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        GuardaDatosBitacora(1)
        Me.CONPARCIALESTableAdapter.Connection = CON
        Me.CONPARCIALESTableAdapter.Delete(Consecutivo)
        CON.Close()
        BOREntregasParcialesDolares(Consecutivo)
        GloBnd = True
        MsgBox("Los Datos Fueron Borrados Satisfactoriamente", MsgBoxStyle.Information, "Borrar")
        Me.Close()

    End Sub

    Private Sub ComboBox1_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ComboBox1.SelectedIndexChanged

    End Sub

    Private Sub ChequesTextBox_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles ChequesTextBox.KeyPress
        e.KeyChar = Chr((ValidaKey(Me.ChequesTextBox, Asc(LCase(e.KeyChar)), "M")))
    End Sub

    Private Sub ChequesTextBox_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ChequesTextBox.TextChanged
        Suma_Detalle()

    End Sub

    Private Sub TarjetaTextBox_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles TarjetaTextBox.KeyPress
        e.KeyChar = Chr((ValidaKey(Me.TarjetaTextBox, Asc(LCase(e.KeyChar)), "M")))
    End Sub

    Private Sub TarjetaTextBox_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TarjetaTextBox.TextChanged
        Suma_Detalle()
    End Sub

    Private Sub CONEntregasParcialesDolares(ByVal Consecutivo As Integer)
        Dim dTable As New DataTable
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@Consecutivo", SqlDbType.Int, Consecutivo)
        dTable = BaseII.ConsultaDT("CONEntregasParcialesDolares")
        If dTable.Rows.Count = 0 Then Exit Sub
        tbImporteDolares.Text = dTable.Rows(0)(0).ToString
        tbTipoCambio.Text = dTable.Rows(0)(1).ToString
        tbConversionPesos.Text = dTable.Rows(0)(2).ToString
    End Sub

    Private Sub NUEEntregasParcialesDolares(ByVal Consecutivo As Integer, ByVal ImporteDolares As Decimal, ByVal TipoCambio As Decimal)
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@Consecutivo", SqlDbType.Int, Consecutivo)
        BaseII.CreateMyParameter("@ImporteDolares", SqlDbType.Decimal, ImporteDolares)
        BaseII.CreateMyParameter("@TipoCambio", SqlDbType.Decimal, TipoCambio)
        BaseII.Inserta("NUEEntregasParcialesDolares")
    End Sub

    Private Sub BOREntregasParcialesDolares(ByVal Consecutivo As Integer)
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@Consecutivo", SqlDbType.Int, Consecutivo)
        BaseII.Inserta("BOREntregasParcialesDolares")
    End Sub

    Private Sub tbImporteDolares_KeyPress(sender As System.Object, e As System.Windows.Forms.KeyPressEventArgs) Handles tbImporteDolares.KeyPress
        e.KeyChar = Chr((ValidaKey(tbImporteDolares, Asc(LCase(e.KeyChar)), "M")))
    End Sub

    Private Sub tbTipoCambio_KeyPress(sender As System.Object, e As System.Windows.Forms.KeyPressEventArgs) Handles tbTipoCambio.KeyPress
        e.KeyChar = Chr((ValidaKey(tbTipoCambio, Asc(LCase(e.KeyChar)), "M")))
    End Sub

    Private Sub tbImporteDolares_TextChanged(sender As System.Object, e As System.EventArgs) Handles tbImporteDolares.TextChanged
        Suma_Detalle()
    End Sub

    Private Sub tbTipoCambio_TextChanged(sender As System.Object, e As System.EventArgs) Handles tbTipoCambio.TextChanged
        Suma_Detalle()
    End Sub
End Class