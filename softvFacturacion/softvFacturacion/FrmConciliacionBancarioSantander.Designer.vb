<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmConciliacionBancarioSantander
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim CMBNombreLabel As System.Windows.Forms.Label
        Dim Clv_sessionLabel As System.Windows.Forms.Label
        Dim ClienteLabel As System.Windows.Forms.Label
        Dim Clv_facturaLabel As System.Windows.Forms.Label
        Dim StatusLabel As System.Windows.Forms.Label
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle3 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(FrmConciliacionBancarioSantander))
        Me.Button6 = New System.Windows.Forms.Button
        Me.Button5 = New System.Windows.Forms.Button
        Me.Button3 = New System.Windows.Forms.Button
        Me.Button2 = New System.Windows.Forms.Button
        Me.TextBox1 = New System.Windows.Forms.TextBox
        Me.CMBLabel2 = New System.Windows.Forms.Label
        Me.CMBLabel1 = New System.Windows.Forms.Label
        Me.Busca_conciliacion_santaderDataGridView = New System.Windows.Forms.DataGridView
        Me.DataGridViewTextBoxColumn1 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn2 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn3 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn4 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Importe = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Status = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Cancelada = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewCheckBoxColumn1 = New System.Windows.Forms.DataGridViewCheckBoxColumn
        Me.detalle = New System.Windows.Forms.DataGridViewButtonColumn
        Me.Busca_conciliacion_santaderBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.ProcedimientosArnoldo3 = New softvFacturacion.ProcedimientosArnoldo3
        Me.DateTimePicker1 = New System.Windows.Forms.DateTimePicker
        Me.CMBLabel3 = New System.Windows.Forms.Label
        Me.CMBPanel1 = New System.Windows.Forms.Panel
        Me.CMBLabel5 = New System.Windows.Forms.Label
        Me.Button7 = New System.Windows.Forms.Button
        Me.ComboBox1 = New System.Windows.Forms.ComboBox
        Me.MuestraStatusConciliacionBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.ProcedimientosArnoldo3BindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Button1 = New System.Windows.Forms.Button
        Me.CMBNombreLabel1 = New System.Windows.Forms.Label
        Me.Busca_conciliacion_santaderTableAdapter = New softvFacturacion.ProcedimientosArnoldo3TableAdapters.Busca_conciliacion_santaderTableAdapter
        Me.Muestra_Status_ConciliacionTableAdapter = New softvFacturacion.ProcedimientosArnoldo3TableAdapters.Muestra_Status_ConciliacionTableAdapter
        Me.Button4 = New System.Windows.Forms.Button
        Me.Clv_sessionTextBox = New System.Windows.Forms.TextBox
        Me.ClienteTextBox = New System.Windows.Forms.TextBox
        Me.OpenFileDialog1 = New System.Windows.Forms.OpenFileDialog
        Me.Clv_facturaTextBox = New System.Windows.Forms.TextBox
        Me.StatusTextBox = New System.Windows.Forms.TextBox
        Me.CMBPanel3 = New System.Windows.Forms.Panel
        Me.BindingNavigator1 = New System.Windows.Forms.BindingNavigator(Me.components)
        Me.BindingNavigatorSeparator2 = New System.Windows.Forms.ToolStripSeparator
        Me.GuardarToolStripButton = New System.Windows.Forms.ToolStripButton
        Me.tsbEliminar = New System.Windows.Forms.ToolStripButton
        CMBNombreLabel = New System.Windows.Forms.Label
        Clv_sessionLabel = New System.Windows.Forms.Label
        ClienteLabel = New System.Windows.Forms.Label
        Clv_facturaLabel = New System.Windows.Forms.Label
        StatusLabel = New System.Windows.Forms.Label
        CType(Me.Busca_conciliacion_santaderDataGridView, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Busca_conciliacion_santaderBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ProcedimientosArnoldo3, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.CMBPanel1.SuspendLayout()
        CType(Me.MuestraStatusConciliacionBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ProcedimientosArnoldo3BindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.CMBPanel3.SuspendLayout()
        CType(Me.BindingNavigator1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.BindingNavigator1.SuspendLayout()
        Me.SuspendLayout()
        '
        'CMBNombreLabel
        '
        CMBNombreLabel.AutoSize = True
        CMBNombreLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        CMBNombreLabel.ForeColor = System.Drawing.Color.DarkRed
        CMBNombreLabel.Location = New System.Drawing.Point(233, 95)
        CMBNombreLabel.Name = "CMBNombreLabel"
        CMBNombreLabel.Size = New System.Drawing.Size(169, 20)
        CMBNombreLabel.TabIndex = 30
        CMBNombreLabel.Text = "Nombre Del Cliente:"
        '
        'Clv_sessionLabel
        '
        Clv_sessionLabel.AutoSize = True
        Clv_sessionLabel.Location = New System.Drawing.Point(94, 853)
        Clv_sessionLabel.Name = "Clv_sessionLabel"
        Clv_sessionLabel.Size = New System.Drawing.Size(62, 13)
        Clv_sessionLabel.TabIndex = 32
        Clv_sessionLabel.Text = "clv session:"
        '
        'ClienteLabel
        '
        ClienteLabel.AutoSize = True
        ClienteLabel.Location = New System.Drawing.Point(357, 850)
        ClienteLabel.Name = "ClienteLabel"
        ClienteLabel.Size = New System.Drawing.Size(41, 13)
        ClienteLabel.TabIndex = 33
        ClienteLabel.Text = "cliente:"
        '
        'Clv_facturaLabel
        '
        Clv_facturaLabel.AutoSize = True
        Clv_facturaLabel.Location = New System.Drawing.Point(569, 847)
        Clv_facturaLabel.Name = "Clv_facturaLabel"
        Clv_facturaLabel.Size = New System.Drawing.Size(60, 13)
        Clv_facturaLabel.TabIndex = 35
        Clv_facturaLabel.Text = "clv factura:"
        '
        'StatusLabel
        '
        StatusLabel.AutoSize = True
        StatusLabel.Location = New System.Drawing.Point(780, 847)
        StatusLabel.Name = "StatusLabel"
        StatusLabel.Size = New System.Drawing.Size(40, 13)
        StatusLabel.TabIndex = 36
        StatusLabel.Text = "Status:"
        '
        'Button6
        '
        Me.Button6.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button6.Location = New System.Drawing.Point(745, 141)
        Me.Button6.Name = "Button6"
        Me.Button6.Size = New System.Drawing.Size(238, 33)
        Me.Button6.TabIndex = 19
        Me.Button6.Text = "IMPRIMIR &DETALLE"
        Me.Button6.UseVisualStyleBackColor = True
        '
        'Button5
        '
        Me.Button5.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button5.Location = New System.Drawing.Point(745, 102)
        Me.Button5.Name = "Button5"
        Me.Button5.Size = New System.Drawing.Size(238, 33)
        Me.Button5.TabIndex = 18
        Me.Button5.Text = "&IMPRIMIR FACTURAS"
        Me.Button5.UseVisualStyleBackColor = True
        Me.Button5.Visible = False
        '
        'Button3
        '
        Me.Button3.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button3.Location = New System.Drawing.Point(745, 63)
        Me.Button3.Name = "Button3"
        Me.Button3.Size = New System.Drawing.Size(238, 33)
        Me.Button3.TabIndex = 16
        Me.Button3.Text = "&AFECTAR CLIENTES"
        Me.Button3.UseVisualStyleBackColor = True
        '
        'Button2
        '
        Me.Button2.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button2.Location = New System.Drawing.Point(809, 697)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(174, 30)
        Me.Button2.TabIndex = 20
        Me.Button2.Text = "&SALIR"
        Me.Button2.UseVisualStyleBackColor = True
        '
        'TextBox1
        '
        Me.TextBox1.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextBox1.Location = New System.Drawing.Point(17, 118)
        Me.TextBox1.Name = "TextBox1"
        Me.TextBox1.Size = New System.Drawing.Size(135, 24)
        Me.TextBox1.TabIndex = 23
        '
        'CMBLabel2
        '
        Me.CMBLabel2.AutoSize = True
        Me.CMBLabel2.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBLabel2.Location = New System.Drawing.Point(14, 153)
        Me.CMBLabel2.Name = "CMBLabel2"
        Me.CMBLabel2.Size = New System.Drawing.Size(59, 18)
        Me.CMBLabel2.TabIndex = 27
        Me.CMBLabel2.Text = "Fecha:"
        '
        'CMBLabel1
        '
        Me.CMBLabel1.AutoSize = True
        Me.CMBLabel1.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBLabel1.Location = New System.Drawing.Point(14, 97)
        Me.CMBLabel1.Name = "CMBLabel1"
        Me.CMBLabel1.Size = New System.Drawing.Size(79, 18)
        Me.CMBLabel1.TabIndex = 26
        Me.CMBLabel1.Text = "Contrato:"
        '
        'Busca_conciliacion_santaderDataGridView
        '
        Me.Busca_conciliacion_santaderDataGridView.AutoGenerateColumns = False
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.Busca_conciliacion_santaderDataGridView.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle1
        Me.Busca_conciliacion_santaderDataGridView.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.DataGridViewTextBoxColumn1, Me.DataGridViewTextBoxColumn2, Me.DataGridViewTextBoxColumn3, Me.DataGridViewTextBoxColumn4, Me.Importe, Me.Status, Me.Cancelada, Me.DataGridViewCheckBoxColumn1, Me.detalle})
        Me.Busca_conciliacion_santaderDataGridView.DataSource = Me.Busca_conciliacion_santaderBindingSource
        DataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.Busca_conciliacion_santaderDataGridView.DefaultCellStyle = DataGridViewCellStyle3
        Me.Busca_conciliacion_santaderDataGridView.Location = New System.Drawing.Point(31, 269)
        Me.Busca_conciliacion_santaderDataGridView.Name = "Busca_conciliacion_santaderDataGridView"
        Me.Busca_conciliacion_santaderDataGridView.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.Busca_conciliacion_santaderDataGridView.Size = New System.Drawing.Size(952, 416)
        Me.Busca_conciliacion_santaderDataGridView.TabIndex = 27
        '
        'DataGridViewTextBoxColumn1
        '
        Me.DataGridViewTextBoxColumn1.DataPropertyName = "cliente"
        Me.DataGridViewTextBoxColumn1.HeaderText = "Cliente"
        Me.DataGridViewTextBoxColumn1.Name = "DataGridViewTextBoxColumn1"
        Me.DataGridViewTextBoxColumn1.ReadOnly = True
        '
        'DataGridViewTextBoxColumn2
        '
        Me.DataGridViewTextBoxColumn2.DataPropertyName = "Nombre"
        Me.DataGridViewTextBoxColumn2.HeaderText = "Nombre Del Cliente"
        Me.DataGridViewTextBoxColumn2.Name = "DataGridViewTextBoxColumn2"
        Me.DataGridViewTextBoxColumn2.ReadOnly = True
        Me.DataGridViewTextBoxColumn2.Width = 400
        '
        'DataGridViewTextBoxColumn3
        '
        Me.DataGridViewTextBoxColumn3.DataPropertyName = "Fecha"
        Me.DataGridViewTextBoxColumn3.HeaderText = "Fecha"
        Me.DataGridViewTextBoxColumn3.Name = "DataGridViewTextBoxColumn3"
        Me.DataGridViewTextBoxColumn3.ReadOnly = True
        '
        'DataGridViewTextBoxColumn4
        '
        Me.DataGridViewTextBoxColumn4.DataPropertyName = "Hora"
        Me.DataGridViewTextBoxColumn4.HeaderText = "Hora"
        Me.DataGridViewTextBoxColumn4.Name = "DataGridViewTextBoxColumn4"
        Me.DataGridViewTextBoxColumn4.ReadOnly = True
        '
        'Importe
        '
        Me.Importe.DataPropertyName = "Importe"
        DataGridViewCellStyle2.Format = "C2"
        DataGridViewCellStyle2.NullValue = "0"
        Me.Importe.DefaultCellStyle = DataGridViewCellStyle2
        Me.Importe.HeaderText = "Importe"
        Me.Importe.Name = "Importe"
        Me.Importe.ReadOnly = True
        '
        'Status
        '
        Me.Status.DataPropertyName = "Status"
        Me.Status.HeaderText = "Status"
        Me.Status.Name = "Status"
        Me.Status.ReadOnly = True
        '
        'Cancelada
        '
        Me.Cancelada.DataPropertyName = "Cancelada"
        Me.Cancelada.HeaderText = "Status Del Proceso"
        Me.Cancelada.Name = "Cancelada"
        Me.Cancelada.ReadOnly = True
        '
        'DataGridViewCheckBoxColumn1
        '
        Me.DataGridViewCheckBoxColumn1.DataPropertyName = "autorizado"
        Me.DataGridViewCheckBoxColumn1.HeaderText = "Autorizado"
        Me.DataGridViewCheckBoxColumn1.Name = "DataGridViewCheckBoxColumn1"
        '
        'detalle
        '
        Me.detalle.DataPropertyName = "detalle"
        Me.detalle.HeaderText = "detalle"
        Me.detalle.Name = "detalle"
        '
        'Busca_conciliacion_santaderBindingSource
        '
        Me.Busca_conciliacion_santaderBindingSource.DataMember = "Busca_conciliacion_santader"
        Me.Busca_conciliacion_santaderBindingSource.DataSource = Me.ProcedimientosArnoldo3
        '
        'ProcedimientosArnoldo3
        '
        Me.ProcedimientosArnoldo3.DataSetName = "ProcedimientosArnoldo3"
        Me.ProcedimientosArnoldo3.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'DateTimePicker1
        '
        Me.DateTimePicker1.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.DateTimePicker1.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.DateTimePicker1.Location = New System.Drawing.Point(17, 174)
        Me.DateTimePicker1.Name = "DateTimePicker1"
        Me.DateTimePicker1.Size = New System.Drawing.Size(135, 24)
        Me.DateTimePicker1.TabIndex = 28
        '
        'CMBLabel3
        '
        Me.CMBLabel3.AutoSize = True
        Me.CMBLabel3.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBLabel3.Location = New System.Drawing.Point(14, 5)
        Me.CMBLabel3.Name = "CMBLabel3"
        Me.CMBLabel3.Size = New System.Drawing.Size(187, 18)
        Me.CMBLabel3.TabIndex = 29
        Me.CMBLabel3.Text = "Opciones de Búsqueda:"
        '
        'CMBPanel1
        '
        Me.CMBPanel1.Controls.Add(Me.CMBLabel5)
        Me.CMBPanel1.Controls.Add(Me.Button7)
        Me.CMBPanel1.Controls.Add(Me.ComboBox1)
        Me.CMBPanel1.Controls.Add(Me.TextBox1)
        Me.CMBPanel1.Controls.Add(Me.CMBLabel3)
        Me.CMBPanel1.Controls.Add(Me.Button1)
        Me.CMBPanel1.Controls.Add(Me.DateTimePicker1)
        Me.CMBPanel1.Controls.Add(Me.CMBLabel1)
        Me.CMBPanel1.Controls.Add(Me.CMBLabel2)
        Me.CMBPanel1.Controls.Add(Me.CMBNombreLabel1)
        Me.CMBPanel1.Controls.Add(CMBNombreLabel)
        Me.CMBPanel1.Location = New System.Drawing.Point(31, 12)
        Me.CMBPanel1.Name = "CMBPanel1"
        Me.CMBPanel1.Size = New System.Drawing.Size(708, 208)
        Me.CMBPanel1.TabIndex = 30
        '
        'CMBLabel5
        '
        Me.CMBLabel5.AutoSize = True
        Me.CMBLabel5.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBLabel5.Location = New System.Drawing.Point(14, 37)
        Me.CMBLabel5.Name = "CMBLabel5"
        Me.CMBLabel5.Size = New System.Drawing.Size(172, 18)
        Me.CMBLabel5.TabIndex = 32
        Me.CMBLabel5.Text = "Seleccione un Status:"
        '
        'Button7
        '
        Me.Button7.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button7.Location = New System.Drawing.Point(167, 173)
        Me.Button7.Name = "Button7"
        Me.Button7.Size = New System.Drawing.Size(89, 26)
        Me.Button7.TabIndex = 30
        Me.Button7.Text = "&BUSCAR"
        Me.Button7.UseVisualStyleBackColor = True
        '
        'ComboBox1
        '
        Me.ComboBox1.DataSource = Me.MuestraStatusConciliacionBindingSource
        Me.ComboBox1.DisplayMember = "Descripcion"
        Me.ComboBox1.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ComboBox1.FormattingEnabled = True
        Me.ComboBox1.Location = New System.Drawing.Point(17, 58)
        Me.ComboBox1.Name = "ComboBox1"
        Me.ComboBox1.Size = New System.Drawing.Size(269, 26)
        Me.ComboBox1.TabIndex = 31
        Me.ComboBox1.ValueMember = "clave"
        '
        'MuestraStatusConciliacionBindingSource
        '
        Me.MuestraStatusConciliacionBindingSource.DataMember = "Muestra_Status_Conciliacion"
        Me.MuestraStatusConciliacionBindingSource.DataSource = Me.ProcedimientosArnoldo3BindingSource
        '
        'ProcedimientosArnoldo3BindingSource
        '
        Me.ProcedimientosArnoldo3BindingSource.DataSource = Me.ProcedimientosArnoldo3
        Me.ProcedimientosArnoldo3BindingSource.Position = 0
        '
        'Button1
        '
        Me.Button1.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button1.Location = New System.Drawing.Point(167, 118)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(34, 26)
        Me.Button1.TabIndex = 21
        Me.Button1.Text = "&..."
        Me.Button1.UseVisualStyleBackColor = True
        '
        'CMBNombreLabel1
        '
        Me.CMBNombreLabel1.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.Busca_conciliacion_santaderBindingSource, "Nombre", True))
        Me.CMBNombreLabel1.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBNombreLabel1.ForeColor = System.Drawing.Color.DarkRed
        Me.CMBNombreLabel1.Location = New System.Drawing.Point(233, 118)
        Me.CMBNombreLabel1.Name = "CMBNombreLabel1"
        Me.CMBNombreLabel1.Size = New System.Drawing.Size(472, 26)
        Me.CMBNombreLabel1.TabIndex = 31
        '
        'Busca_conciliacion_santaderTableAdapter
        '
        Me.Busca_conciliacion_santaderTableAdapter.ClearBeforeFill = True
        '
        'Muestra_Status_ConciliacionTableAdapter
        '
        Me.Muestra_Status_ConciliacionTableAdapter.ClearBeforeFill = True
        '
        'Button4
        '
        Me.Button4.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button4.Location = New System.Drawing.Point(745, 24)
        Me.Button4.Name = "Button4"
        Me.Button4.Size = New System.Drawing.Size(238, 33)
        Me.Button4.TabIndex = 32
        Me.Button4.Text = "&CARGAR ARCHIVO"
        Me.Button4.UseVisualStyleBackColor = True
        '
        'Clv_sessionTextBox
        '
        Me.Clv_sessionTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.Busca_conciliacion_santaderBindingSource, "clv_session", True))
        Me.Clv_sessionTextBox.Location = New System.Drawing.Point(162, 850)
        Me.Clv_sessionTextBox.Name = "Clv_sessionTextBox"
        Me.Clv_sessionTextBox.Size = New System.Drawing.Size(100, 20)
        Me.Clv_sessionTextBox.TabIndex = 33
        '
        'ClienteTextBox
        '
        Me.ClienteTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.Busca_conciliacion_santaderBindingSource, "cliente", True))
        Me.ClienteTextBox.Location = New System.Drawing.Point(404, 847)
        Me.ClienteTextBox.Name = "ClienteTextBox"
        Me.ClienteTextBox.Size = New System.Drawing.Size(100, 20)
        Me.ClienteTextBox.TabIndex = 34
        '
        'OpenFileDialog1
        '
        Me.OpenFileDialog1.FileName = "OpenFileDialog1"
        '
        'Clv_facturaTextBox
        '
        Me.Clv_facturaTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.Busca_conciliacion_santaderBindingSource, "clv_factura", True))
        Me.Clv_facturaTextBox.Location = New System.Drawing.Point(635, 844)
        Me.Clv_facturaTextBox.Name = "Clv_facturaTextBox"
        Me.Clv_facturaTextBox.Size = New System.Drawing.Size(100, 20)
        Me.Clv_facturaTextBox.TabIndex = 36
        '
        'StatusTextBox
        '
        Me.StatusTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.Busca_conciliacion_santaderBindingSource, "Status", True))
        Me.StatusTextBox.Location = New System.Drawing.Point(826, 844)
        Me.StatusTextBox.Name = "StatusTextBox"
        Me.StatusTextBox.Size = New System.Drawing.Size(100, 20)
        Me.StatusTextBox.TabIndex = 37
        '
        'CMBPanel3
        '
        Me.CMBPanel3.Controls.Add(Me.BindingNavigator1)
        Me.CMBPanel3.Location = New System.Drawing.Point(31, 232)
        Me.CMBPanel3.Name = "CMBPanel3"
        Me.CMBPanel3.Size = New System.Drawing.Size(964, 31)
        Me.CMBPanel3.TabIndex = 38
        '
        'BindingNavigator1
        '
        Me.BindingNavigator1.AddNewItem = Nothing
        Me.BindingNavigator1.CountItem = Nothing
        Me.BindingNavigator1.DeleteItem = Nothing
        Me.BindingNavigator1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.BindingNavigatorSeparator2, Me.GuardarToolStripButton, Me.tsbEliminar})
        Me.BindingNavigator1.Location = New System.Drawing.Point(0, 0)
        Me.BindingNavigator1.MoveFirstItem = Nothing
        Me.BindingNavigator1.MoveLastItem = Nothing
        Me.BindingNavigator1.MoveNextItem = Nothing
        Me.BindingNavigator1.MovePreviousItem = Nothing
        Me.BindingNavigator1.Name = "BindingNavigator1"
        Me.BindingNavigator1.PositionItem = Nothing
        Me.BindingNavigator1.Size = New System.Drawing.Size(964, 25)
        Me.BindingNavigator1.TabIndex = 0
        Me.BindingNavigator1.Text = "BindingNavigator1"
        '
        'BindingNavigatorSeparator2
        '
        Me.BindingNavigatorSeparator2.Name = "BindingNavigatorSeparator2"
        Me.BindingNavigatorSeparator2.Size = New System.Drawing.Size(6, 25)
        '
        'GuardarToolStripButton
        '
        Me.GuardarToolStripButton.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right
        Me.GuardarToolStripButton.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GuardarToolStripButton.Image = CType(resources.GetObject("GuardarToolStripButton.Image"), System.Drawing.Image)
        Me.GuardarToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.GuardarToolStripButton.Name = "GuardarToolStripButton"
        Me.GuardarToolStripButton.Size = New System.Drawing.Size(129, 22)
        Me.GuardarToolStripButton.Text = "&Guardar Detalle"
        '
        'tsbEliminar
        '
        Me.tsbEliminar.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right
        Me.tsbEliminar.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Bold)
        Me.tsbEliminar.Image = CType(resources.GetObject("tsbEliminar.Image"), System.Drawing.Image)
        Me.tsbEliminar.Name = "tsbEliminar"
        Me.tsbEliminar.RightToLeftAutoMirrorImage = True
        Me.tsbEliminar.Size = New System.Drawing.Size(81, 22)
        Me.tsbEliminar.Text = "&Eliminar"
        '
        'FrmConciliacionBancarioSantander
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1004, 738)
        Me.Controls.Add(Me.CMBPanel3)
        Me.Controls.Add(StatusLabel)
        Me.Controls.Add(Me.StatusTextBox)
        Me.Controls.Add(Clv_facturaLabel)
        Me.Controls.Add(Me.Clv_facturaTextBox)
        Me.Controls.Add(ClienteLabel)
        Me.Controls.Add(Me.ClienteTextBox)
        Me.Controls.Add(Clv_sessionLabel)
        Me.Controls.Add(Me.Clv_sessionTextBox)
        Me.Controls.Add(Me.Button4)
        Me.Controls.Add(Me.CMBPanel1)
        Me.Controls.Add(Me.Busca_conciliacion_santaderDataGridView)
        Me.Controls.Add(Me.Button2)
        Me.Controls.Add(Me.Button6)
        Me.Controls.Add(Me.Button5)
        Me.Controls.Add(Me.Button3)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "FrmConciliacionBancarioSantander"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Conciliación Bancaria Bancomer"
        CType(Me.Busca_conciliacion_santaderDataGridView, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Busca_conciliacion_santaderBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ProcedimientosArnoldo3, System.ComponentModel.ISupportInitialize).EndInit()
        Me.CMBPanel1.ResumeLayout(False)
        Me.CMBPanel1.PerformLayout()
        CType(Me.MuestraStatusConciliacionBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ProcedimientosArnoldo3BindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.CMBPanel3.ResumeLayout(False)
        Me.CMBPanel3.PerformLayout()
        CType(Me.BindingNavigator1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.BindingNavigator1.ResumeLayout(False)
        Me.BindingNavigator1.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Button6 As System.Windows.Forms.Button
    Friend WithEvents Button5 As System.Windows.Forms.Button
    Friend WithEvents Button3 As System.Windows.Forms.Button
    Friend WithEvents Button2 As System.Windows.Forms.Button
    Friend WithEvents TextBox1 As System.Windows.Forms.TextBox
    Friend WithEvents CMBLabel2 As System.Windows.Forms.Label
    Friend WithEvents CMBLabel1 As System.Windows.Forms.Label
    Friend WithEvents ProcedimientosArnoldo3 As softvFacturacion.ProcedimientosArnoldo3
    Friend WithEvents Busca_conciliacion_santaderBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents Busca_conciliacion_santaderTableAdapter As softvFacturacion.ProcedimientosArnoldo3TableAdapters.Busca_conciliacion_santaderTableAdapter
    Friend WithEvents Busca_conciliacion_santaderDataGridView As System.Windows.Forms.DataGridView
    Friend WithEvents DateTimePicker1 As System.Windows.Forms.DateTimePicker
    Friend WithEvents CMBLabel3 As System.Windows.Forms.Label
    Friend WithEvents CMBPanel1 As System.Windows.Forms.Panel
    Friend WithEvents Button7 As System.Windows.Forms.Button
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents ComboBox1 As System.Windows.Forms.ComboBox
    Friend WithEvents CMBLabel5 As System.Windows.Forms.Label
    Friend WithEvents MuestraStatusConciliacionBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents ProcedimientosArnoldo3BindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents Muestra_Status_ConciliacionTableAdapter As softvFacturacion.ProcedimientosArnoldo3TableAdapters.Muestra_Status_ConciliacionTableAdapter
    Friend WithEvents CMBNombreLabel1 As System.Windows.Forms.Label
    Friend WithEvents Button4 As System.Windows.Forms.Button
    Friend WithEvents Clv_sessionTextBox As System.Windows.Forms.TextBox
    Friend WithEvents ClienteTextBox As System.Windows.Forms.TextBox
    Friend WithEvents OpenFileDialog1 As System.Windows.Forms.OpenFileDialog
    Friend WithEvents Clv_facturaTextBox As System.Windows.Forms.TextBox
    Friend WithEvents DataGridViewTextBoxColumn1 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn2 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn3 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn4 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Importe As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Status As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Cancelada As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewCheckBoxColumn1 As System.Windows.Forms.DataGridViewCheckBoxColumn
    Friend WithEvents detalle As System.Windows.Forms.DataGridViewButtonColumn
    Friend WithEvents StatusTextBox As System.Windows.Forms.TextBox
    Friend WithEvents CMBPanel3 As System.Windows.Forms.Panel
    Friend WithEvents BindingNavigator1 As System.Windows.Forms.BindingNavigator
    Friend WithEvents BindingNavigatorSeparator2 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents GuardarToolStripButton As System.Windows.Forms.ToolStripButton
    Friend WithEvents tsbEliminar As System.Windows.Forms.ToolStripButton
End Class
