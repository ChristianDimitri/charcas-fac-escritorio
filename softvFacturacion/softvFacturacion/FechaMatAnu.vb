﻿Imports CrystalDecisions.Shared
Imports CrystalDecisions.CrystalReports.Engine
Imports System.Data.SqlClient
Imports System.Text

Public Class FechaMatAnu

    Private Sub FechaMatAnu_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        colorea(Me)

    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click

        ReporteMateAnu(Me.DTimeIni.Value, Me.DTimeFin.Value)

    End Sub

    Public Sub ReporteMateAnu(ByVal FechaIni As Date, ByVal FechaFin As Date)

        Dim conexion As New SqlConnection(MiConexion)
        conexion.Open()

        Dim rDocument As New ReportDocument
        Dim dSet As New DataSet
        Dim tableNameList As New List(Of String)
        tableNameList.Add("ReporteMaterialAnualidad")
        tableNameList.Add("General")
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@FechaIni", SqlDbType.DateTime, FechaIni)
        BaseII.CreateMyParameter("@FechaFin", SqlDbType.DateTime, FechaFin)
        dSet = BaseII.ConsultaDS("ReporteMaterialAnualidad", tableNameList)

        rDocument = New ReportDocument
        rDocument.Load(RutaReportes + "\ReporteMaterialAnualidad.rpt")
        rDocument.SetDataSource(dSet)

        'rDocument.PrintOptions.PrinterName = "LASER"
        'rDocument.PrintToPrinter(1, False, 1, 5)

    End Sub
End Class