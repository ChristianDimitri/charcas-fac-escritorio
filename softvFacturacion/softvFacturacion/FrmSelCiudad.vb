Imports System.Data.SqlClient

Public Class FrmSelCiudad

    Private Sub FrmSelCiudad_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            colorea(Me)
            gloClv_Session = 0
            Dim CON As New SqlConnection(MiConexion)
            CON.Open()
            Me.DameClv_Session_ServiciosTableAdapter.Connection = CON
            Me.DameClv_Session_ServiciosTableAdapter.Fill(Me.DataSetEdgar.DameClv_Session_Servicios, gloClv_Session)
            Me.MuestraSelecciona_CiudadTmpNUEVOTableAdapter.Connection = CON
            Me.MuestraSelecciona_CiudadTmpNUEVOTableAdapter.Fill(Me.DataSetEdgar.MuestraSelecciona_CiudadTmpNUEVO, New System.Nullable(Of Long)(CType(gloClv_Session, Long)))
            Me.MuestraSeleccion_CiudadCONSULTATableAdapter.Connection = CON
            Me.MuestraSeleccion_CiudadCONSULTATableAdapter.Fill(Me.DataSetEdgar.MuestraSeleccion_CiudadCONSULTA, gloClv_Session)
            Me.MuestraSelecciona_CiudadTmpCONSULTATableAdapter.Connection = CON
            Me.MuestraSelecciona_CiudadTmpCONSULTATableAdapter.Fill(Me.DataSetEdgar.MuestraSelecciona_CiudadTmpCONSULTA, gloClv_Session)
            CON.Close()
        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub Button3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button3.Click
        Try
            Dim CON As New SqlConnection(MiConexion)
            CON.Open()
            Me.PONTODOSSelecciona_CiudadTmp_Seleccion_CiudadTableAdapter.Connection = CON
            Me.PONTODOSSelecciona_CiudadTmp_Seleccion_CiudadTableAdapter.Fill(Me.DataSetEdgar.PONTODOSSelecciona_CiudadTmp_Seleccion_Ciudad, New System.Nullable(Of Long)(CType(gloClv_Session, Long)))
            Me.MuestraSeleccion_CiudadCONSULTATableAdapter.Connection = CON
            Me.MuestraSeleccion_CiudadCONSULTATableAdapter.Fill(Me.DataSetEdgar.MuestraSeleccion_CiudadCONSULTA, gloClv_Session)
            Me.MuestraSelecciona_CiudadTmpCONSULTATableAdapter.Connection = CON
            Me.MuestraSelecciona_CiudadTmpCONSULTATableAdapter.Fill(Me.DataSetEdgar.MuestraSelecciona_CiudadTmpCONSULTA, gloClv_Session)
            CON.Close()
        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub Button4_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button4.Click
        Try
            If IsNumeric(Me.Clv_ciudadTextBox1.Text) = True Then
                If Me.Clv_ciudadTextBox1.Text > 0 Then
                    Dim CON As New SqlConnection(MiConexion)
                    CON.Open()
                    Me.PONUNOSelecciona_CiudadTmp_Seleccion_CiudadTableAdapter.Connection = CON
                    Me.PONUNOSelecciona_CiudadTmp_Seleccion_CiudadTableAdapter.Fill(Me.DataSetEdgar.PONUNOSelecciona_CiudadTmp_Seleccion_Ciudad, New System.Nullable(Of Long)(CType(gloClv_Session, Long)), New System.Nullable(Of Integer)(CType(Me.Clv_ciudadTextBox1.Text, Integer)))
                    Me.MuestraSeleccion_CiudadCONSULTATableAdapter.Connection = CON
                    Me.MuestraSeleccion_CiudadCONSULTATableAdapter.Fill(Me.DataSetEdgar.MuestraSeleccion_CiudadCONSULTA, gloClv_Session)
                    Me.MuestraSelecciona_CiudadTmpCONSULTATableAdapter.Connection = CON
                    Me.MuestraSelecciona_CiudadTmpCONSULTATableAdapter.Fill(Me.DataSetEdgar.MuestraSelecciona_CiudadTmpCONSULTA, gloClv_Session)
                    CON.Close()
                End If
            End If

        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        Try
            If IsNumeric(Me.Clv_CiudadTextBox.Text) = True Then
                If Me.Clv_CiudadTextBox.Text > 0 Then
                    Dim CON As New SqlConnection(MiConexion)
                    CON.Open()
                    Me.PONUNOSeleccion_Ciudad_Selecciona_CiudadTmpTableAdapter.Connection = CON
                    Me.PONUNOSeleccion_Ciudad_Selecciona_CiudadTmpTableAdapter.Fill(Me.DataSetEdgar.PONUNOSeleccion_Ciudad_Selecciona_CiudadTmp, New System.Nullable(Of Long)(CType(gloClv_Session, Long)), New System.Nullable(Of Integer)(CType(Me.Clv_CiudadTextBox.Text, Integer)))
                    Me.MuestraSeleccion_CiudadCONSULTATableAdapter.Connection = CON
                    Me.MuestraSeleccion_CiudadCONSULTATableAdapter.Fill(Me.DataSetEdgar.MuestraSeleccion_CiudadCONSULTA, gloClv_Session)
                    Me.MuestraSelecciona_CiudadTmpCONSULTATableAdapter.Connection = CON
                    Me.MuestraSelecciona_CiudadTmpCONSULTATableAdapter.Fill(Me.DataSetEdgar.MuestraSelecciona_CiudadTmpCONSULTA, gloClv_Session)
                    CON.Close()
                End If
            End If
        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        Try
            Dim CON As New SqlConnection(MiConexion)
            CON.Open()
            Me.PONTODOSSeleccion_Ciudad_Selecciona_CiudadTmpTableAdapter.Connection = CON
            Me.PONTODOSSeleccion_Ciudad_Selecciona_CiudadTmpTableAdapter.Fill(Me.DataSetEdgar.PONTODOSSeleccion_Ciudad_Selecciona_CiudadTmp, New System.Nullable(Of Long)(CType(gloClv_Session, Long)))
            Me.MuestraSeleccion_CiudadCONSULTATableAdapter.Connection = CON
            Me.MuestraSeleccion_CiudadCONSULTATableAdapter.Fill(Me.DataSetEdgar.MuestraSeleccion_CiudadCONSULTA, gloClv_Session)
            Me.MuestraSelecciona_CiudadTmpCONSULTATableAdapter.Connection = CON
            Me.MuestraSelecciona_CiudadTmpCONSULTATableAdapter.Fill(Me.DataSetEdgar.MuestraSelecciona_CiudadTmpCONSULTA, gloClv_Session)
            CON.Close()

        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try

    End Sub


    Private Sub Button5_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button5.Click
        ciudades = 0
        Me.Close()
    End Sub

    Private Sub Button6_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button6.Click
        'MsgBox(Me.DataGridView2.RowCount)
        ciudades = 0
        Try
            For i As Integer = 0 To DataGridView2.RowCount - 1
                ciudades = ciudades + CInt(DataGridView2.Item(2, i).Value)
            Next
        Catch ex As Exception
            MsgBox(ex.Message.ToString)
        End Try
        'MsgBox(ciudades)
        If Me.DataGridView2.RowCount > 0 Then
            If GloBnd_Des_Men = True Then
                FrmFechaIngresosConcepto.Show()
                Me.Close()
            ElseIf LocbndPolizaCiudad = True Then
                FrmFechaIngresosConcepto.Show()
                Me.Close()
            Else
                GloBnd_Des_Men = False
                GloBnd_Des_Cont = False
                LocbndPolizaCiudad = False
                Dim CON As New SqlConnection(MiConexion)
                CON.Open()
                Me.Dame_ciudad_carteraTableAdapter.Connection = CON
                Me.Dame_ciudad_carteraTableAdapter.Fill(Me.Procedimientos_arnoldo.Dame_ciudad_cartera, gloClv_Session, LocCiudades)
                CON.Close()
                FrmFechaIngresosConcepto.Show()
                Me.Close()
                ''pruebas
            End If
        Else
            MsgBox("Seleccione alguna Ciudad ", MsgBoxStyle.Information)
        End If
    End Sub

End Class