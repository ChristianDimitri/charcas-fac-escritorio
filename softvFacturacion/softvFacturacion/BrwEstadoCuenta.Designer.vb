﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class BrwEstadoCuenta
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle3 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle4 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle5 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle7 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle8 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle6 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(BrwEstadoCuenta))
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.dgEstadoCuentaStatus = New System.Windows.Forms.DataGridView()
        Me.bnVerTodos = New System.Windows.Forms.Button()
        Me.dgEstadoCuenta = New System.Windows.Forms.DataGridView()
        Me.IdEstadoCuenta = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Contrato = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Nombre = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.TotalAPagar = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.bnVer = New System.Windows.Forms.Button()
        Me.bnSalir = New System.Windows.Forms.Button()
        Me.CMBLabel1 = New System.Windows.Forms.Label()
        Me.CMBLabel2 = New System.Windows.Forms.Label()
        Me.pbProcesando = New System.Windows.Forms.PictureBox()
        Me.CMBProcesando = New System.Windows.Forms.Label()
        Me.FolderBrowserDialog1 = New System.Windows.Forms.FolderBrowserDialog()
        Me.Clv_Periodo = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Fecha = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Periodo = New System.Windows.Forms.DataGridViewTextBoxColumn()
        CType(Me.dgEstadoCuentaStatus, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dgEstadoCuenta, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.pbProcesando, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'dgEstadoCuentaStatus
        '
        Me.dgEstadoCuentaStatus.AllowUserToAddRows = False
        Me.dgEstadoCuentaStatus.AllowUserToDeleteRows = False
        Me.dgEstadoCuentaStatus.BackgroundColor = System.Drawing.Color.WhiteSmoke
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgEstadoCuentaStatus.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle1
        Me.dgEstadoCuentaStatus.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgEstadoCuentaStatus.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.Clv_Periodo, Me.Fecha, Me.Periodo})
        DataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgEstadoCuentaStatus.DefaultCellStyle = DataGridViewCellStyle3
        Me.dgEstadoCuentaStatus.Location = New System.Drawing.Point(540, 36)
        Me.dgEstadoCuentaStatus.Name = "dgEstadoCuentaStatus"
        Me.dgEstadoCuentaStatus.ReadOnly = True
        DataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle4.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle4.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle4.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle4.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle4.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgEstadoCuentaStatus.RowHeadersDefaultCellStyle = DataGridViewCellStyle4
        Me.dgEstadoCuentaStatus.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgEstadoCuentaStatus.Size = New System.Drawing.Size(295, 232)
        Me.dgEstadoCuentaStatus.TabIndex = 0
        '
        'bnVerTodos
        '
        Me.bnVerTodos.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.bnVerTodos.Location = New System.Drawing.Point(841, 36)
        Me.bnVerTodos.Name = "bnVerTodos"
        Me.bnVerTodos.Size = New System.Drawing.Size(114, 28)
        Me.bnVerTodos.TabIndex = 1
        Me.bnVerTodos.Text = "Exportar Todos"
        Me.bnVerTodos.UseVisualStyleBackColor = True
        '
        'dgEstadoCuenta
        '
        Me.dgEstadoCuenta.AllowUserToAddRows = False
        Me.dgEstadoCuenta.AllowUserToDeleteRows = False
        Me.dgEstadoCuenta.BackgroundColor = System.Drawing.Color.WhiteSmoke
        DataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle5.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle5.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle5.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle5.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle5.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle5.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgEstadoCuenta.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle5
        Me.dgEstadoCuenta.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgEstadoCuenta.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.IdEstadoCuenta, Me.Contrato, Me.Nombre, Me.TotalAPagar})
        DataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle7.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle7.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle7.ForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle7.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle7.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle7.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgEstadoCuenta.DefaultCellStyle = DataGridViewCellStyle7
        Me.dgEstadoCuenta.Location = New System.Drawing.Point(70, 297)
        Me.dgEstadoCuenta.Name = "dgEstadoCuenta"
        Me.dgEstadoCuenta.ReadOnly = True
        DataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle8.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle8.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle8.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle8.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle8.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle8.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgEstadoCuenta.RowHeadersDefaultCellStyle = DataGridViewCellStyle8
        Me.dgEstadoCuenta.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgEstadoCuenta.Size = New System.Drawing.Size(765, 332)
        Me.dgEstadoCuenta.TabIndex = 2
        '
        'IdEstadoCuenta
        '
        Me.IdEstadoCuenta.DataPropertyName = "IdEstadoCuenta"
        Me.IdEstadoCuenta.HeaderText = "IdEstadoCuenta"
        Me.IdEstadoCuenta.Name = "IdEstadoCuenta"
        Me.IdEstadoCuenta.ReadOnly = True
        Me.IdEstadoCuenta.Visible = False
        '
        'Contrato
        '
        Me.Contrato.DataPropertyName = "Contrato"
        Me.Contrato.HeaderText = "Contrato"
        Me.Contrato.Name = "Contrato"
        Me.Contrato.ReadOnly = True
        '
        'Nombre
        '
        Me.Nombre.DataPropertyName = "Nombre"
        Me.Nombre.HeaderText = "Nombre"
        Me.Nombre.Name = "Nombre"
        Me.Nombre.ReadOnly = True
        Me.Nombre.Width = 450
        '
        'TotalAPagar
        '
        Me.TotalAPagar.DataPropertyName = "TotalAPagar"
        DataGridViewCellStyle6.Format = "C2"
        DataGridViewCellStyle6.NullValue = Nothing
        Me.TotalAPagar.DefaultCellStyle = DataGridViewCellStyle6
        Me.TotalAPagar.HeaderText = "TotalAPagar"
        Me.TotalAPagar.Name = "TotalAPagar"
        Me.TotalAPagar.ReadOnly = True
        Me.TotalAPagar.Width = 150
        '
        'bnVer
        '
        Me.bnVer.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.bnVer.Location = New System.Drawing.Point(841, 297)
        Me.bnVer.Name = "bnVer"
        Me.bnVer.Size = New System.Drawing.Size(114, 28)
        Me.bnVer.TabIndex = 3
        Me.bnVer.Text = "Exportar"
        Me.bnVer.UseVisualStyleBackColor = True
        '
        'bnSalir
        '
        Me.bnSalir.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.bnSalir.Location = New System.Drawing.Point(860, 682)
        Me.bnSalir.Name = "bnSalir"
        Me.bnSalir.Size = New System.Drawing.Size(136, 36)
        Me.bnSalir.TabIndex = 4
        Me.bnSalir.Text = "&SALIR"
        Me.bnSalir.UseVisualStyleBackColor = True
        '
        'CMBLabel1
        '
        Me.CMBLabel1.AutoSize = True
        Me.CMBLabel1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBLabel1.Location = New System.Drawing.Point(537, 18)
        Me.CMBLabel1.Name = "CMBLabel1"
        Me.CMBLabel1.Size = New System.Drawing.Size(295, 15)
        Me.CMBLabel1.TabIndex = 5
        Me.CMBLabel1.Text = "Fechas de Generación de Estados de Cuenta"
        '
        'CMBLabel2
        '
        Me.CMBLabel2.AutoSize = True
        Me.CMBLabel2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBLabel2.Location = New System.Drawing.Point(67, 279)
        Me.CMBLabel2.Name = "CMBLabel2"
        Me.CMBLabel2.Size = New System.Drawing.Size(127, 15)
        Me.CMBLabel2.TabIndex = 6
        Me.CMBLabel2.Text = "Estados de Cuenta"
        '
        'pbProcesando
        '
        Me.pbProcesando.Image = CType(resources.GetObject("pbProcesando.Image"), System.Drawing.Image)
        Me.pbProcesando.Location = New System.Drawing.Point(566, 657)
        Me.pbProcesando.Name = "pbProcesando"
        Me.pbProcesando.Size = New System.Drawing.Size(49, 44)
        Me.pbProcesando.TabIndex = 7
        Me.pbProcesando.TabStop = False
        Me.pbProcesando.Visible = False
        '
        'CMBProcesando
        '
        Me.CMBProcesando.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBProcesando.ForeColor = System.Drawing.Color.SteelBlue
        Me.CMBProcesando.Location = New System.Drawing.Point(238, 677)
        Me.CMBProcesando.Name = "CMBProcesando"
        Me.CMBProcesando.Size = New System.Drawing.Size(322, 24)
        Me.CMBProcesando.TabIndex = 8
        Me.CMBProcesando.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.CMBProcesando.Visible = False
        '
        'Clv_Periodo
        '
        Me.Clv_Periodo.DataPropertyName = "Clv_Periodo"
        Me.Clv_Periodo.HeaderText = "Clv_Periodo"
        Me.Clv_Periodo.Name = "Clv_Periodo"
        Me.Clv_Periodo.ReadOnly = True
        Me.Clv_Periodo.Visible = False
        '
        'Fecha
        '
        Me.Fecha.DataPropertyName = "Fecha"
        DataGridViewCellStyle2.Format = "d"
        DataGridViewCellStyle2.NullValue = Nothing
        Me.Fecha.DefaultCellStyle = DataGridViewCellStyle2
        Me.Fecha.HeaderText = "Fecha"
        Me.Fecha.Name = "Fecha"
        Me.Fecha.ReadOnly = True
        Me.Fecha.Width = 230
        '
        'Periodo
        '
        Me.Periodo.DataPropertyName = "Periodo"
        Me.Periodo.HeaderText = "Periodo"
        Me.Periodo.Name = "Periodo"
        Me.Periodo.ReadOnly = True
        Me.Periodo.Visible = False
        Me.Periodo.Width = 150
        '
        'BrwEstadoCuenta
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.White
        Me.ClientSize = New System.Drawing.Size(1008, 730)
        Me.Controls.Add(Me.CMBProcesando)
        Me.Controls.Add(Me.pbProcesando)
        Me.Controls.Add(Me.CMBLabel2)
        Me.Controls.Add(Me.CMBLabel1)
        Me.Controls.Add(Me.bnSalir)
        Me.Controls.Add(Me.bnVer)
        Me.Controls.Add(Me.dgEstadoCuenta)
        Me.Controls.Add(Me.bnVerTodos)
        Me.Controls.Add(Me.dgEstadoCuentaStatus)
        Me.Name = "BrwEstadoCuenta"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Estados de Cuenta"
        CType(Me.dgEstadoCuentaStatus, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dgEstadoCuenta, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.pbProcesando, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents dgEstadoCuentaStatus As System.Windows.Forms.DataGridView
    Friend WithEvents bnVerTodos As System.Windows.Forms.Button
    Friend WithEvents dgEstadoCuenta As System.Windows.Forms.DataGridView
    Friend WithEvents bnVer As System.Windows.Forms.Button
    Friend WithEvents bnSalir As System.Windows.Forms.Button
    Friend WithEvents IdEstadoCuenta As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Contrato As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Nombre As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents TotalAPagar As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents CMBLabel1 As System.Windows.Forms.Label
    Friend WithEvents CMBLabel2 As System.Windows.Forms.Label
    Friend WithEvents pbProcesando As System.Windows.Forms.PictureBox
    Friend WithEvents CMBProcesando As System.Windows.Forms.Label
    Friend WithEvents FolderBrowserDialog1 As System.Windows.Forms.FolderBrowserDialog
    Friend WithEvents Clv_Periodo As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Fecha As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Periodo As System.Windows.Forms.DataGridViewTextBoxColumn
End Class
